-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `facturi_detalii`
--

DROP TABLE IF EXISTS `facturi_detalii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facturi_detalii` (
  `Cod_Detalii` int NOT NULL AUTO_INCREMENT,
  `Nr_Crt` int DEFAULT NULL,
  `Denumire` varchar(20) DEFAULT NULL,
  `Cantitate_Ore` int DEFAULT NULL,
  `Pretul_Unitar` double(10,2) DEFAULT NULL,
  `Comentarii` varchar(300) DEFAULT NULL,
  `Serie_Numar` varchar(8) NOT NULL,
  `Id_Serviciu` int DEFAULT NULL,
  PRIMARY KEY (`Cod_Detalii`),
  KEY `fk_facturi_detalii1` (`Serie_Numar`),
  KEY `fk_facturi_detalii2` (`Id_Serviciu`),
  CONSTRAINT `fk_facturi_detalii1` FOREIGN KEY (`Serie_Numar`) REFERENCES `facturi` (`Serie_Numar`) ON DELETE CASCADE,
  CONSTRAINT `fk_facturi_detalii2` FOREIGN KEY (`Id_Serviciu`) REFERENCES `dictionar_servicii` (`Id_Serviciu`) ON DELETE CASCADE,
  CONSTRAINT `facturi_detalii_chk_1` CHECK ((`Nr_Crt` > 0)),
  CONSTRAINT `facturi_detalii_chk_2` CHECK ((`Cantitate_Ore` > 0))
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturi_detalii`
--

LOCK TABLES `facturi_detalii` WRITE;
/*!40000 ALTER TABLE `facturi_detalii` DISABLE KEYS */;
INSERT INTO `facturi_detalii` VALUES (1,1,'AVANS',1,200.00,NULL,'2020/1',NULL),(2,1,'Development',8,90.00,NULL,'2020/2',1),(4,1,'AVANS',1,300.00,NULL,'2020/3',NULL),(5,1,'Development',10,90.00,NULL,'2020/4',1),(7,1,'AVANS',1,500.00,NULL,'2020/5',NULL),(8,1,'Support',30,50.00,NULL,'2020/6',2),(10,1,'AVANS',1,500.00,NULL,'2020/13',NULL),(11,1,'Support',30,50.00,NULL,'2020/14',2),(13,1,'AVANS',1,200.00,NULL,'2020/15',NULL),(14,1,'Development',8,90.00,NULL,'2020/16',1),(16,1,'AVANS',1,200.00,NULL,'2020/17',NULL),(17,1,'Development',8,90.00,NULL,'2020/18',1),(19,1,'AVANS',1,500.00,NULL,'2020/19',NULL),(20,1,'Support',30,50.00,NULL,'2020/20',2),(22,1,'AVANS',1,150.00,NULL,'2020/21',NULL),(23,1,'Development',9,90.00,NULL,'2020/22',1),(25,1,'AVANS',1,200.00,NULL,'2020/23',NULL),(26,1,'Development',8,90.00,NULL,'2020/24',1),(28,1,'AVANS',1,230.00,NULL,'2020/25',NULL),(29,1,'Support',17,50.00,NULL,'2020/26',2),(31,1,'AVANS',1,500.00,NULL,'2020/27',NULL),(32,1,'Support',30,50.00,NULL,'2020/28',2),(34,1,'AVANS',1,1600.00,NULL,'2020/29',NULL),(35,1,'Implemetare',70,40.00,NULL,'2020/30',3),(37,1,'AVANS',1,220.00,NULL,'2020/31',NULL),(38,1,'Support',17,50.00,NULL,'2020/32',2),(40,1,'Support',30,50.00,NULL,'2020/8',2),(42,1,'AVANS',1,700.00,NULL,'2020/9',NULL),(43,1,'Implemetare',45,40.00,NULL,'2020/10',3),(45,1,'AVANS',1,500.00,NULL,'2020/7',NULL),(46,1,'AVANS',1,300.00,NULL,'2020/11',NULL),(47,1,'Implemetare',15,40.00,NULL,'2020/12',3),(49,1,'Support',4,45.00,'das','2020/33',2),(50,2,'Penalizari',5,0.00,'dddddd','2020/33',4),(51,1,'Support',1,50.00,'fff','2020/34',2),(52,1,'Support',1,50.00,'','2020/35',2),(53,2,'Altele',2,3.00,'','2020/35',5),(54,3,'Implemetare',2,40.00,'','2020/35',3),(55,1,'Implemetare',1,40.00,'','2020/36',3),(56,2,'Implemetare',1,400.00,'','2020/36',3),(57,3,'Penalizari',5,12.00,'','2020/36',4),(58,1,'Implemetare',1,40.00,'','2020/37',3),(59,1,'Implemetare',1,40.00,'','2020/38',3),(60,1,'Implemetare',1,40.00,'','2020/39',3),(61,1,'Support',23,50.00,'aasd','2020/40',2),(62,2,'Support',2,50.00,'','2020/40',2);
/*!40000 ALTER TABLE `facturi_detalii` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:55:31
