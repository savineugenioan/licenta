-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `achizitii_detalii`
--

DROP TABLE IF EXISTS `achizitii_detalii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `achizitii_detalii` (
  `Cod_Detalii` int NOT NULL AUTO_INCREMENT,
  `Produsul` varchar(20) DEFAULT NULL,
  `U_M` varchar(5) NOT NULL,
  `Cantitatea` int DEFAULT NULL,
  `Pretul_Unitar` double(6,2) DEFAULT NULL,
  `Pret` double(10,2) DEFAULT NULL,
  `Serie_Numar` varchar(8) NOT NULL,
  PRIMARY KEY (`Cod_Detalii`),
  KEY `fk_achiz_det` (`Serie_Numar`),
  CONSTRAINT `fk_achiz_det` FOREIGN KEY (`Serie_Numar`) REFERENCES `achizitii` (`Serie_Numar`) ON DELETE CASCADE,
  CONSTRAINT `achizitii_detalii_chk_1` CHECK ((`Pretul_unitar` > 0)),
  CONSTRAINT `achizitii_detalii_chk_2` CHECK ((`Pret` > 0))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achizitii_detalii`
--

LOCK TABLES `achizitii_detalii` WRITE;
/*!40000 ALTER TABLE `achizitii_detalii` DISABLE KEYS */;
INSERT INTO `achizitii_detalii` VALUES (1,'Calculatoare','buc',30,24.00,720.00,'2020/1'),(2,'Calculatoare','buc',30,24.00,720.00,'2020/2'),(3,'Licenta ORACLE','buc',1,300.00,300.00,'2020/3'),(4,'Placa Video','buc',30,24.00,720.00,'2020/4'),(5,'Calculatoare','buc',30,24.00,720.00,'2020/5');
/*!40000 ALTER TABLE `achizitii_detalii` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:55:31
