-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contracte`
--

DROP TABLE IF EXISTS `contracte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contracte` (
  `Nr_Contract` int NOT NULL AUTO_INCREMENT,
  `Data_Incheierii` date NOT NULL,
  `Avans` int DEFAULT NULL,
  `Avans_Platit` int DEFAULT '0',
  `Rest_Plata` int DEFAULT NULL,
  `Contract_Platit` int DEFAULT '0',
  `Data_Plata_Avans` date DEFAULT NULL,
  `Data_Plata_Rest` date DEFAULT NULL,
  `Cod_Client` int DEFAULT NULL,
  PRIMARY KEY (`Nr_Contract`),
  KEY `fk_Contracte` (`Cod_Client`),
  CONSTRAINT `fk_Contracte` FOREIGN KEY (`Cod_Client`) REFERENCES `clienti` (`Cod_Client`) ON DELETE CASCADE,
  CONSTRAINT `contracte_chk_1` CHECK ((`Avans` >= 0)),
  CONSTRAINT `contracte_chk_2` CHECK ((`Avans_Platit` in (0,1))),
  CONSTRAINT `contracte_chk_3` CHECK ((`Contract_Platit` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contracte`
--

LOCK TABLES `contracte` WRITE;
/*!40000 ALTER TABLE `contracte` DISABLE KEYS */;
INSERT INTO `contracte` VALUES (1,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',1),(2,'2020-04-03',300,0,900,0,'2020-06-12','2020-07-02',1),(3,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',1),(4,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',2),(5,'2020-04-03',700,0,1800,0,'2020-06-12','2020-07-02',3),(6,'2020-04-03',300,0,600,0,'2020-06-12','2020-07-02',3),(7,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',4),(8,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',5),(9,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',5),(10,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',1),(11,'2020-04-03',150,0,810,0,'2020-06-12','2020-07-02',6),(12,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',1),(13,'2020-04-03',230,0,765,0,'2020-06-12','2020-07-02',14),(14,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',1),(15,'2020-04-03',1600,0,3500,0,'2020-06-12','2020-07-02',1),(16,'2020-04-03',220,0,765,0,'2020-06-12','2020-07-02',14);
/*!40000 ALTER TABLE `contracte` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:55:31
