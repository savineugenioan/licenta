-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dictionar_servicii`
--

DROP TABLE IF EXISTS `dictionar_servicii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dictionar_servicii` (
  `Id_Serviciu` int NOT NULL AUTO_INCREMENT,
  `Cod_Serviciu` int DEFAULT NULL,
  `Denumire` varchar(40) NOT NULL,
  `Cota_TVA` double(4,3) DEFAULT '0.190',
  `Pretul_Ora` int DEFAULT NULL,
  `Pretul_Ora_CFidel` int DEFAULT NULL,
  PRIMARY KEY (`Id_Serviciu`),
  CONSTRAINT `dictionar_servicii_chk_1` CHECK ((`Pretul_Ora` >= 0)),
  CONSTRAINT `dictionar_servicii_chk_2` CHECK ((`Pretul_Ora_CFidel` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dictionar_servicii`
--

LOCK TABLES `dictionar_servicii` WRITE;
/*!40000 ALTER TABLE `dictionar_servicii` DISABLE KEYS */;
INSERT INTO `dictionar_servicii` VALUES (1,1,'Development',0.190,90,85),(2,2,'Support',0.190,50,45),(3,3,'Implemetare',0.190,40,35),(4,4,'Penalizari',0.000,0,0),(5,5,'Altele',0.000,0,0);
/*!40000 ALTER TABLE `dictionar_servicii` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:55:30
