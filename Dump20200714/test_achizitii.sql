-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `achizitii`
--

DROP TABLE IF EXISTS `achizitii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `achizitii` (
  `Serie_Numar` varchar(8) NOT NULL,
  `Data_achizitie` date DEFAULT NULL,
  `Valoare_Achizitie` double(8,2) DEFAULT NULL,
  `Nr_Contract` int NOT NULL,
  PRIMARY KEY (`Serie_Numar`),
  KEY `fk_achizitii` (`Nr_Contract`),
  CONSTRAINT `fk_achizitii` FOREIGN KEY (`Nr_Contract`) REFERENCES `contracte` (`Nr_Contract`) ON DELETE CASCADE,
  CONSTRAINT `achizitii_chk_1` CHECK ((`Valoare_achizitie` > 0))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achizitii`
--

LOCK TABLES `achizitii` WRITE;
/*!40000 ALTER TABLE `achizitii` DISABLE KEYS */;
INSERT INTO `achizitii` VALUES ('2020/1','2020-04-03',720.00,5),('2020/2','2020-04-03',720.00,6),('2020/3','2020-04-03',300.00,12),('2020/4','2020-04-03',720.00,14),('2020/5','2020-04-03',720.00,15);
/*!40000 ALTER TABLE `achizitii` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:55:31
