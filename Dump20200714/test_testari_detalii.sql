-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `testari_detalii`
--

DROP TABLE IF EXISTS `testari_detalii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testari_detalii` (
  `Cod_Detalii` int NOT NULL AUTO_INCREMENT,
  `Procedura` varchar(90) NOT NULL,
  `Rezultatul_Asteptat` varchar(20) DEFAULT NULL,
  `Pass` int DEFAULT NULL,
  `Cod_testare` int NOT NULL,
  PRIMARY KEY (`Cod_Detalii`),
  KEY `fk_testare_detalii` (`Cod_testare`),
  CONSTRAINT `fk_testare_detalii` FOREIGN KEY (`Cod_testare`) REFERENCES `testari` (`Cod_Testare`) ON DELETE CASCADE,
  CONSTRAINT `testari_detalii_chk_1` CHECK ((`Pass` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testari_detalii`
--

LOCK TABLES `testari_detalii` WRITE;
/*!40000 ALTER TABLE `testari_detalii` DISABLE KEYS */;
INSERT INTO `testari_detalii` VALUES (1,'Îndepline?te func?ionalitatea primar? ?i men?ine stabilitatea','DA',1,1),(2,'Functioneaza corect sub un cont de utilizator','DA',1,1),(3,'Functioneaza corect sub un cont de utilizator de admin','DA',1,1),(4,'Functioneaza corect sub un cont de administrator','DA',1,1),(5,'Finalizeaz? o instalare minim?','DA',1,1),(6,'Îndepline?te func?ionalitatea primar? ?i men?ine stabilitatea','DA',1,2),(7,'Functioneaza corect sub un cont de utilizator','DA',1,2),(8,'Functioneaza corect sub un cont de utilizator de admin','DA',1,2),(9,'Functioneaza corect sub un cont de administrator','DA',1,2),(10,'Finalizeaz? o instalare minim?','DA',1,2),(11,'Porne?te de la consol?','DA',1,3),(12,'Porne?te de la fi?ierul autorun pe CD-ul aplica?iei','DA',1,3),(13,'Porne?te dintr-un document sau dintr-un fi?ier (dac? aplica?ia are extensii asociate)','DA',1,3),(14,'Porne?te de la bara Lansare rapid?','DA',1,3),(15,'Finalizeaz? o instalare minim?','DA',1,3),(16,'Completeaz? o instalare custom','DA',1,4),(17,'Completeaz? o instalare complet?','DA',1,4),(18,'Functioneaza corect la nivel de re?ea','DA',1,4),(19,'Finalizeaz? o instalare local? atunci când utiliza?i Ad?ugare sau eliminare programe','DA',1,4),(20,'Finalizeaz? o instalare minim?','DA',1,4),(21,'asdasdasd','DA',0,4);
/*!40000 ALTER TABLE `testari_detalii` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:55:31
