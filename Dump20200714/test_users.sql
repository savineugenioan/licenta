-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `Cod_Utilizator` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL,
  `Parola` varchar(50) NOT NULL,
  `Nume` varchar(50) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `Nivel_Acces` varchar(1) NOT NULL,
  PRIMARY KEY (`Cod_Utilizator`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `Email` (`Email`),
  CONSTRAINT `ck_admin` CHECK ((`Nivel_Acces` in (_utf8mb4'0',_utf8mb4'1',_utf8mb4'2')))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sara','81dc9bdb52d04dc20036dbd8313ed055','sara','sara@yahoo.com','1'),(2,'adrian','81dc9bdb52d04dc20036dbd8313ed055','adrian','adrian@yahoo.com','0'),(3,'vasile','81dc9bdb52d04dc20036dbd8313ed055','vasile','vasile@yahoo.com','0'),(4,'georgeg','81dc9bdb52d04dc20036dbd8313ed055','george','george@yahoo.com','1'),(5,'gigi','81dc9bdb52d04dc20036dbd8313ed055','gigi','gigi@yahoo.com','0'),(6,'vera','81dc9bdb52d04dc20036dbd8313ed055','vera','vera@yahoo.com','0'),(7,'cristina','81dc9bdb52d04dc20036dbd8313ed055','cristina','cristina@yahoo.com','0'),(8,'mircea','81dc9bdb52d04dc20036dbd8313ed055','mircea','mircea@yahoo.com','0'),(9,'admin','21232f297a57a5a743894a0e4a801fc3','admin','admin@yahoo.com','2'),(10,'lidia','81dc9bdb52d04dc20036dbd8313ed055','lidia','lidia@yahoo.com','1'),(11,'asdffff','81dc9bdb52d04dc20036dbd8313ed055','nume_parola_1234','asdas@email.com','0'),(12,'asdc','81dc9bdb52d04dc20036dbd8313ed055','asdc','asdc@email.com','0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:55:30
