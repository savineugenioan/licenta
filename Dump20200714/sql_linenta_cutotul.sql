CREATE DATABASE  IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `test`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `achizitii`
--

DROP TABLE IF EXISTS `achizitii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `achizitii` (
  `Serie_Numar` varchar(8) NOT NULL,
  `Data_achizitie` date DEFAULT NULL,
  `Valoare_Achizitie` double(8,2) DEFAULT NULL,
  `Nr_Contract` int NOT NULL,
  PRIMARY KEY (`Serie_Numar`),
  KEY `fk_achizitii` (`Nr_Contract`),
  CONSTRAINT `fk_achizitii` FOREIGN KEY (`Nr_Contract`) REFERENCES `contracte` (`Nr_Contract`) ON DELETE CASCADE,
  CONSTRAINT `achizitii_chk_1` CHECK ((`Valoare_achizitie` > 0))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achizitii`
--

LOCK TABLES `achizitii` WRITE;
/*!40000 ALTER TABLE `achizitii` DISABLE KEYS */;
INSERT INTO `achizitii` VALUES ('2020/1','2020-04-03',720.00,5),('2020/2','2020-04-03',720.00,6),('2020/3','2020-04-03',300.00,12),('2020/4','2020-04-03',720.00,14),('2020/5','2020-04-03',720.00,15);
/*!40000 ALTER TABLE `achizitii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `achizitii_detalii`
--

DROP TABLE IF EXISTS `achizitii_detalii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `achizitii_detalii` (
  `Cod_Detalii` int NOT NULL AUTO_INCREMENT,
  `Produsul` varchar(20) DEFAULT NULL,
  `U_M` varchar(5) NOT NULL,
  `Cantitatea` int DEFAULT NULL,
  `Pretul_Unitar` double(6,2) DEFAULT NULL,
  `Pret` double(10,2) DEFAULT NULL,
  `Serie_Numar` varchar(8) NOT NULL,
  PRIMARY KEY (`Cod_Detalii`),
  KEY `fk_achiz_det` (`Serie_Numar`),
  CONSTRAINT `fk_achiz_det` FOREIGN KEY (`Serie_Numar`) REFERENCES `achizitii` (`Serie_Numar`) ON DELETE CASCADE,
  CONSTRAINT `achizitii_detalii_chk_1` CHECK ((`Pretul_unitar` > 0)),
  CONSTRAINT `achizitii_detalii_chk_2` CHECK ((`Pret` > 0))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achizitii_detalii`
--

LOCK TABLES `achizitii_detalii` WRITE;
/*!40000 ALTER TABLE `achizitii_detalii` DISABLE KEYS */;
INSERT INTO `achizitii_detalii` VALUES (1,'Calculatoare','buc',30,24.00,720.00,'2020/1'),(2,'Calculatoare','buc',30,24.00,720.00,'2020/2'),(3,'Licenta ORACLE','buc',1,300.00,300.00,'2020/3'),(4,'Placa Video','buc',30,24.00,720.00,'2020/4'),(5,'Calculatoare','buc',30,24.00,720.00,'2020/5');
/*!40000 ALTER TABLE `achizitii_detalii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clienti`
--

DROP TABLE IF EXISTS `clienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clienti` (
  `Cod_Client` int NOT NULL AUTO_INCREMENT,
  `CUI` varchar(10) NOT NULL,
  `Denumire` varchar(70) NOT NULL,
  `Telefon` varchar(15) DEFAULT NULL,
  `Client_Fidel` int DEFAULT '0',
  `Adresa` varchar(100) NOT NULL,
  `IBAN` varchar(34) DEFAULT NULL,
  PRIMARY KEY (`Cod_Client`),
  CONSTRAINT `clienti_chk_1` CHECK ((`Client_Fidel` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienti`
--

LOCK TABLES `clienti` WRITE;
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
INSERT INTO `clienti` VALUES (1,'29477580','BEDELECO EAST EUROPE SRL','0757446576',0,'Aleea Camilla 2,A1','RO682004000000000157841Z'),(2,'23221221','Light Candel ART SRL','0757446576',0,'Aleea Bucuresti 2,A1','RO682004000000000167841Z'),(3,'27438852','STEHOS ACERO SRL','0757446576',1,'Aleea Halelor 2,A1','RO682004000000000157843Z'),(4,'15994196','PROCEMA PERLIT SRL','0757446576',0,'SOS Giurgiului 2,A1','RO682004000000000157842Z'),(5,'18530085','A And C TRANS COMPANY INTERNATIONAL SRL','0757446576',0,'Strada Regina Elena 2,A1','RO682004000500000157841Z'),(6,'30897146','AGGREKO SOUTH EAST EUROPE','0757446576',0,'Strada A.I. Cuza 2,A1','RO682004000000500157841Z'),(7,'33218827','ALEEA GARDEN SRL','0757446576',0,'STR Broscari 108','RO682004002000000157841Z'),(8,'16645658','UNIMASTERS LOGISTIC SRL','0757446576',0,'Drum Garii Odai 1A','RO682004009000000157841Z'),(9,'29189529','FRIGOELECTRIC INSTALATII SRL','0757446576',0,'Intr. Brasov 6','RO682004050000000157841Z'),(10,'6707443','ROBITAL INDUSTRIAL SUPPLIER SRL','0757446576',0,'BD. Biruintei 89','RO682004003000000157841Z'),(11,'6646761','ROLLIT PRODIMPEX SRL','0757446576',0,'Aleea Erou Iancu Nicolae 126','RO682004000056000157841Z'),(12,'30490052','PROFESIONAL SECURITY DIVISION SRL','0757446576',0,'STR. Fortului 113M','RO682404000000000157841Z'),(13,'36710734','GYANA CREAT PROD SRL','0757446576',0,'STR Liviu Rebreanu, nr. 93','RO682004005000060157841Z'),(14,'33364695','FINESTORE DISTRIBUTION SRL','0757446576',0,'Str. Horia, Closca si Crisan, nr.7','RO682004006600000157841Z');
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contracte`
--

DROP TABLE IF EXISTS `contracte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contracte` (
  `Nr_Contract` int NOT NULL AUTO_INCREMENT,
  `Data_Incheierii` date NOT NULL,
  `Avans` int DEFAULT NULL,
  `Avans_Platit` int DEFAULT '0',
  `Rest_Plata` int DEFAULT NULL,
  `Contract_Platit` int DEFAULT '0',
  `Data_Plata_Avans` date DEFAULT NULL,
  `Data_Plata_Rest` date DEFAULT NULL,
  `Cod_Client` int DEFAULT NULL,
  PRIMARY KEY (`Nr_Contract`),
  KEY `fk_Contracte` (`Cod_Client`),
  CONSTRAINT `fk_Contracte` FOREIGN KEY (`Cod_Client`) REFERENCES `clienti` (`Cod_Client`) ON DELETE CASCADE,
  CONSTRAINT `contracte_chk_1` CHECK ((`Avans` >= 0)),
  CONSTRAINT `contracte_chk_2` CHECK ((`Avans_Platit` in (0,1))),
  CONSTRAINT `contracte_chk_3` CHECK ((`Contract_Platit` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contracte`
--

LOCK TABLES `contracte` WRITE;
/*!40000 ALTER TABLE `contracte` DISABLE KEYS */;
INSERT INTO `contracte` VALUES (1,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',1),(2,'2020-04-03',300,0,900,0,'2020-06-12','2020-07-02',1),(3,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',1),(4,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',2),(5,'2020-04-03',700,0,1800,0,'2020-06-12','2020-07-02',3),(6,'2020-04-03',300,0,600,0,'2020-06-12','2020-07-02',3),(7,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',4),(8,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',5),(9,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',5),(10,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',1),(11,'2020-04-03',150,0,810,0,'2020-06-12','2020-07-02',6),(12,'2020-04-03',200,0,720,0,'2020-06-12','2020-07-02',1),(13,'2020-04-03',230,0,765,0,'2020-06-12','2020-07-02',14),(14,'2020-04-03',500,0,1500,0,'2020-06-12','2020-07-02',1),(15,'2020-04-03',1600,0,3500,0,'2020-06-12','2020-07-02',1),(16,'2020-04-03',220,0,765,0,'2020-06-12','2020-07-02',14);
/*!40000 ALTER TABLE `contracte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dictionar_servicii`
--

DROP TABLE IF EXISTS `dictionar_servicii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dictionar_servicii` (
  `Id_Serviciu` int NOT NULL AUTO_INCREMENT,
  `Cod_Serviciu` int DEFAULT NULL,
  `Denumire` varchar(40) NOT NULL,
  `Cota_TVA` double(4,3) DEFAULT '0.190',
  `Pretul_Ora` int DEFAULT NULL,
  `Pretul_Ora_CFidel` int DEFAULT NULL,
  PRIMARY KEY (`Id_Serviciu`),
  CONSTRAINT `dictionar_servicii_chk_1` CHECK ((`Pretul_Ora` >= 0)),
  CONSTRAINT `dictionar_servicii_chk_2` CHECK ((`Pretul_Ora_CFidel` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dictionar_servicii`
--

LOCK TABLES `dictionar_servicii` WRITE;
/*!40000 ALTER TABLE `dictionar_servicii` DISABLE KEYS */;
INSERT INTO `dictionar_servicii` VALUES (1,1,'Development',0.190,90,85),(2,2,'Support',0.190,50,45),(3,3,'Implemetare',0.190,40,35),(4,4,'Penalizari',0.000,0,0),(5,5,'Altele',0.000,0,0);
/*!40000 ALTER TABLE `dictionar_servicii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturi`
--

DROP TABLE IF EXISTS `facturi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facturi` (
  `Serie_Numar` varchar(13) NOT NULL,
  `Total` double(10,2) DEFAULT NULL,
  `Penalizari` double(10,2) DEFAULT '0.00',
  `Tip_Factura` varchar(9) DEFAULT 'STANDARD',
  `Achitat` int DEFAULT '0',
  `Data_Scadenta` date NOT NULL,
  `Nr_Contract` int DEFAULT NULL,
  `Cod_Client` int DEFAULT NULL,
  PRIMARY KEY (`Serie_Numar`),
  KEY `fk_facturi_cr` (`Nr_Contract`),
  KEY `fk_facturi_cl` (`Cod_Client`),
  CONSTRAINT `fk_facturi_cl` FOREIGN KEY (`Cod_Client`) REFERENCES `clienti` (`Cod_Client`) ON DELETE CASCADE,
  CONSTRAINT `fk_facturi_cr` FOREIGN KEY (`Nr_Contract`) REFERENCES `contracte` (`Nr_Contract`) ON DELETE CASCADE,
  CONSTRAINT `facturi_chk_1` CHECK ((`Total` > 0)),
  CONSTRAINT `facturi_chk_2` CHECK ((`Achitat` in (0,1))),
  CONSTRAINT `facturi_chk_3` CHECK ((`Tip_Factura` in (_utf8mb4'AVANS',_utf8mb4'STANDARD',_utf8mb4'STORNARE')))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturi`
--

LOCK TABLES `facturi` WRITE;
/*!40000 ALTER TABLE `facturi` DISABLE KEYS */;
INSERT INTO `facturi` VALUES ('2020/1',200.00,208.00,'AVANS',1,'2020-04-03',1,1),('2020/10',1800.00,3564.00,'STANDARD',0,'2020-04-03',5,3),('2020/11',300.00,594.00,'AVANS',0,'2020-04-03',6,3),('2020/12',600.00,312.00,'STANDARD',1,'2020-04-03',6,3),('2020/13',500.00,520.00,'AVANS',1,'2020-04-03',7,4),('2020/14',1500.00,2970.00,'STANDARD',0,'2020-04-03',7,4),('2020/15',200.00,208.00,'AVANS',1,'2020-04-03',8,5),('2020/16',720.00,1425.60,'STANDARD',0,'2020-04-03',8,5),('2020/17',200.00,208.00,'AVANS',1,'2020-04-03',9,5),('2020/18',720.00,1425.60,'STANDARD',0,'2020-04-03',9,5),('2020/19',500.00,990.00,'AVANS',0,'2020-04-03',10,1),('2020/2',720.00,1425.60,'STANDARD',0,'2020-04-03',1,1),('2020/20',1500.00,2970.00,'STANDARD',0,'2020-04-03',10,1),('2020/21',150.00,297.00,'AVANS',0,'2020-04-03',11,6),('2020/22',810.00,1603.80,'STANDARD',0,'2020-04-03',11,6),('2020/23',200.00,396.00,'AVANS',0,'2020-04-03',12,1),('2020/24',720.00,540.80,'STANDARD',1,'2020-04-03',12,1),('2020/25',230.00,455.40,'AVANS',0,'2020-04-03',13,14),('2020/26',850.00,1683.00,'STANDARD',0,'2020-04-03',13,14),('2020/27',500.00,990.00,'AVANS',0,'2020-04-03',14,1),('2020/28',1500.00,2970.00,'STANDARD',0,'2020-04-03',14,1),('2020/29',1600.00,3168.00,'AVANS',0,'2020-04-03',15,1),('2020/3',300.00,594.00,'AVANS',0,'2020-04-03',2,1),('2020/30',2800.00,5544.00,'STANDARD',0,'2020-04-03',15,1),('2020/31',220.00,435.60,'AVANS',0,'2020-04-03',16,14),('2020/32',850.00,1683.00,'STANDARD',0,'2020-04-03',16,14),('2020/33',180.00,165.60,'STANDARD',0,'2020-05-26',5,3),('2020/34',50.00,46.00,'STANDARD',0,'2020-05-26',10,1),('2020/35',136.00,0.00,'STANDARD',1,'2020-05-26',7,4),('2020/36',500.00,0.00,'STANDARD',1,'2020-05-26',11,6),('2020/37',40.00,34.40,'STANDARD',0,'2020-05-29',15,1),('2020/38',40.00,0.00,'STANDARD',0,'2025-06-03',12,1),('2020/39',40.00,0.00,'STANDARD',0,'2029-06-23',11,6),('2020/4',900.00,1782.00,'STANDARD',0,'2020-04-03',2,1),('2020/40',1250.00,1150.00,'STANDARD',0,'2020-05-26',11,6),('2020/5',500.00,990.00,'AVANS',0,'2020-04-03',3,1),('2020/6',1500.00,2970.00,'STANDARD',0,'2020-04-03',3,1),('2020/7',500.00,990.00,'AVANS',0,'2020-04-03',4,2),('2020/8',1500.00,2970.00,'STANDARD',0,'2020-04-03',4,2),('2020/9',700.00,1386.00,'AVANS',0,'2020-04-03',5,3),('S_2020/13',500.00,520.00,'STORNARE',1,'2020-04-03',7,4),('S_2020/15',200.00,208.00,'STORNARE',1,'2020-04-03',8,5);
/*!40000 ALTER TABLE `facturi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturi_detalii`
--

DROP TABLE IF EXISTS `facturi_detalii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facturi_detalii` (
  `Cod_Detalii` int NOT NULL AUTO_INCREMENT,
  `Nr_Crt` int DEFAULT NULL,
  `Denumire` varchar(20) DEFAULT NULL,
  `Cantitate_Ore` int DEFAULT NULL,
  `Pretul_Unitar` double(10,2) DEFAULT NULL,
  `Comentarii` varchar(300) DEFAULT NULL,
  `Serie_Numar` varchar(8) NOT NULL,
  `Id_Serviciu` int DEFAULT NULL,
  PRIMARY KEY (`Cod_Detalii`),
  KEY `fk_facturi_detalii1` (`Serie_Numar`),
  KEY `fk_facturi_detalii2` (`Id_Serviciu`),
  CONSTRAINT `fk_facturi_detalii1` FOREIGN KEY (`Serie_Numar`) REFERENCES `facturi` (`Serie_Numar`) ON DELETE CASCADE,
  CONSTRAINT `fk_facturi_detalii2` FOREIGN KEY (`Id_Serviciu`) REFERENCES `dictionar_servicii` (`Id_Serviciu`) ON DELETE CASCADE,
  CONSTRAINT `facturi_detalii_chk_1` CHECK ((`Nr_Crt` > 0)),
  CONSTRAINT `facturi_detalii_chk_2` CHECK ((`Cantitate_Ore` > 0))
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturi_detalii`
--

LOCK TABLES `facturi_detalii` WRITE;
/*!40000 ALTER TABLE `facturi_detalii` DISABLE KEYS */;
INSERT INTO `facturi_detalii` VALUES (1,1,'AVANS',1,200.00,NULL,'2020/1',NULL),(2,1,'Development',8,90.00,NULL,'2020/2',1),(4,1,'AVANS',1,300.00,NULL,'2020/3',NULL),(5,1,'Development',10,90.00,NULL,'2020/4',1),(7,1,'AVANS',1,500.00,NULL,'2020/5',NULL),(8,1,'Support',30,50.00,NULL,'2020/6',2),(10,1,'AVANS',1,500.00,NULL,'2020/13',NULL),(11,1,'Support',30,50.00,NULL,'2020/14',2),(13,1,'AVANS',1,200.00,NULL,'2020/15',NULL),(14,1,'Development',8,90.00,NULL,'2020/16',1),(16,1,'AVANS',1,200.00,NULL,'2020/17',NULL),(17,1,'Development',8,90.00,NULL,'2020/18',1),(19,1,'AVANS',1,500.00,NULL,'2020/19',NULL),(20,1,'Support',30,50.00,NULL,'2020/20',2),(22,1,'AVANS',1,150.00,NULL,'2020/21',NULL),(23,1,'Development',9,90.00,NULL,'2020/22',1),(25,1,'AVANS',1,200.00,NULL,'2020/23',NULL),(26,1,'Development',8,90.00,NULL,'2020/24',1),(28,1,'AVANS',1,230.00,NULL,'2020/25',NULL),(29,1,'Support',17,50.00,NULL,'2020/26',2),(31,1,'AVANS',1,500.00,NULL,'2020/27',NULL),(32,1,'Support',30,50.00,NULL,'2020/28',2),(34,1,'AVANS',1,1600.00,NULL,'2020/29',NULL),(35,1,'Implemetare',70,40.00,NULL,'2020/30',3),(37,1,'AVANS',1,220.00,NULL,'2020/31',NULL),(38,1,'Support',17,50.00,NULL,'2020/32',2),(40,1,'Support',30,50.00,NULL,'2020/8',2),(42,1,'AVANS',1,700.00,NULL,'2020/9',NULL),(43,1,'Implemetare',45,40.00,NULL,'2020/10',3),(45,1,'AVANS',1,500.00,NULL,'2020/7',NULL),(46,1,'AVANS',1,300.00,NULL,'2020/11',NULL),(47,1,'Implemetare',15,40.00,NULL,'2020/12',3),(49,1,'Support',4,45.00,'das','2020/33',2),(50,2,'Penalizari',5,0.00,'dddddd','2020/33',4),(51,1,'Support',1,50.00,'fff','2020/34',2),(52,1,'Support',1,50.00,'','2020/35',2),(53,2,'Altele',2,3.00,'','2020/35',5),(54,3,'Implemetare',2,40.00,'','2020/35',3),(55,1,'Implemetare',1,40.00,'','2020/36',3),(56,2,'Implemetare',1,400.00,'','2020/36',3),(57,3,'Penalizari',5,12.00,'','2020/36',4),(58,1,'Implemetare',1,40.00,'','2020/37',3),(59,1,'Implemetare',1,40.00,'','2020/38',3),(60,1,'Implemetare',1,40.00,'','2020/39',3),(61,1,'Support',23,50.00,'aasd','2020/40',2),(62,2,'Support',2,50.00,'','2020/40',2);
/*!40000 ALTER TABLE `facturi_detalii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testari`
--

DROP TABLE IF EXISTS `testari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testari` (
  `Cod_Testare` int NOT NULL AUTO_INCREMENT,
  `Data_test` date DEFAULT NULL,
  `Nr_Contract` int NOT NULL,
  PRIMARY KEY (`Cod_Testare`),
  KEY `fk_testare` (`Nr_Contract`),
  CONSTRAINT `fk_testare` FOREIGN KEY (`Nr_Contract`) REFERENCES `contracte` (`Nr_Contract`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testari`
--

LOCK TABLES `testari` WRITE;
/*!40000 ALTER TABLE `testari` DISABLE KEYS */;
INSERT INTO `testari` VALUES (1,'2020-04-03',1),(2,'2020-04-03',2),(3,'2020-04-03',4),(4,'2020-04-03',6);
/*!40000 ALTER TABLE `testari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testari_detalii`
--

DROP TABLE IF EXISTS `testari_detalii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testari_detalii` (
  `Cod_Detalii` int NOT NULL AUTO_INCREMENT,
  `Procedura` varchar(90) NOT NULL,
  `Rezultatul_Asteptat` varchar(20) DEFAULT NULL,
  `Pass` int DEFAULT NULL,
  `Cod_testare` int NOT NULL,
  PRIMARY KEY (`Cod_Detalii`),
  KEY `fk_testare_detalii` (`Cod_testare`),
  CONSTRAINT `fk_testare_detalii` FOREIGN KEY (`Cod_testare`) REFERENCES `testari` (`Cod_Testare`) ON DELETE CASCADE,
  CONSTRAINT `testari_detalii_chk_1` CHECK ((`Pass` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testari_detalii`
--

LOCK TABLES `testari_detalii` WRITE;
/*!40000 ALTER TABLE `testari_detalii` DISABLE KEYS */;
INSERT INTO `testari_detalii` VALUES (1,'Îndepline?te func?ionalitatea primar? ?i men?ine stabilitatea','DA',1,1),(2,'Functioneaza corect sub un cont de utilizator','DA',1,1),(3,'Functioneaza corect sub un cont de utilizator de admin','DA',1,1),(4,'Functioneaza corect sub un cont de administrator','DA',1,1),(5,'Finalizeaz? o instalare minim?','DA',1,1),(6,'Îndepline?te func?ionalitatea primar? ?i men?ine stabilitatea','DA',1,2),(7,'Functioneaza corect sub un cont de utilizator','DA',1,2),(8,'Functioneaza corect sub un cont de utilizator de admin','DA',1,2),(9,'Functioneaza corect sub un cont de administrator','DA',1,2),(10,'Finalizeaz? o instalare minim?','DA',1,2),(11,'Porne?te de la consol?','DA',1,3),(12,'Porne?te de la fi?ierul autorun pe CD-ul aplica?iei','DA',1,3),(13,'Porne?te dintr-un document sau dintr-un fi?ier (dac? aplica?ia are extensii asociate)','DA',1,3),(14,'Porne?te de la bara Lansare rapid?','DA',1,3),(15,'Finalizeaz? o instalare minim?','DA',1,3),(16,'Completeaz? o instalare custom','DA',1,4),(17,'Completeaz? o instalare complet?','DA',1,4),(18,'Functioneaza corect la nivel de re?ea','DA',1,4),(19,'Finalizeaz? o instalare local? atunci când utiliza?i Ad?ugare sau eliminare programe','DA',1,4),(20,'Finalizeaz? o instalare minim?','DA',1,4),(21,'asdasdasd','DA',0,4);
/*!40000 ALTER TABLE `testari_detalii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `Cod_Utilizator` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL,
  `Parola` varchar(50) NOT NULL,
  `Nume` varchar(50) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `Nivel_Acces` varchar(1) NOT NULL,
  PRIMARY KEY (`Cod_Utilizator`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `Email` (`Email`),
  CONSTRAINT `ck_admin` CHECK ((`Nivel_Acces` in (_utf8mb4'0',_utf8mb4'1',_utf8mb4'2')))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sara','81dc9bdb52d04dc20036dbd8313ed055','sara','sara@yahoo.com','1'),(2,'adrian','81dc9bdb52d04dc20036dbd8313ed055','adrian','adrian@yahoo.com','0'),(3,'vasile','81dc9bdb52d04dc20036dbd8313ed055','vasile','vasile@yahoo.com','0'),(4,'georgeg','81dc9bdb52d04dc20036dbd8313ed055','george','george@yahoo.com','1'),(5,'gigi','81dc9bdb52d04dc20036dbd8313ed055','gigi','gigi@yahoo.com','0'),(6,'vera','81dc9bdb52d04dc20036dbd8313ed055','vera','vera@yahoo.com','0'),(7,'cristina','81dc9bdb52d04dc20036dbd8313ed055','cristina','cristina@yahoo.com','0'),(8,'mircea','81dc9bdb52d04dc20036dbd8313ed055','mircea','mircea@yahoo.com','0'),(9,'admin','21232f297a57a5a743894a0e4a801fc3','admin','admin@yahoo.com','2'),(10,'lidia','81dc9bdb52d04dc20036dbd8313ed055','lidia','lidia@yahoo.com','1'),(11,'asdffff','81dc9bdb52d04dc20036dbd8313ed055','nume_parola_1234','asdas@email.com','0'),(12,'asdc','81dc9bdb52d04dc20036dbd8313ed055','asdc','asdc@email.com','0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'test'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `Upgrade_Penalizari_Facturi` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `Upgrade_Penalizari_Facturi` ON SCHEDULE EVERY 1 DAY STARTS '2020-04-04 02:07:45' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE test.FACTURI SET Penalizari = datediff(CURDATE(),Data_Scadenta)*Total*0.02 WHERE Data_Scadenta < curdate() AND Achitat = 0 */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'test'
--
/*!50003 DROP PROCEDURE IF EXISTS `Adaugare_Achizitii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Adaugare_Achizitii`(IN serie_numar VARCHAR(10), IN data_c VARCHAR(10),
 IN valoare_achizitie VARCHAR(10),IN nr_contract VARCHAR(10))
BEGIN
	INSERT INTO ACHIZITII VALUES(serie_numar,data_c,valoare_achizitie,nr_contract);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Adaugare_Achizitii_Detalii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Adaugare_Achizitii_Detalii`(IN Produsul VARCHAR(50), IN U_M VARCHAR(50),
 IN Cantitatea VARCHAR(50),IN Pretul_Unitar VARCHAR(50),IN Pret VARCHAR(50),IN Serie_Numar VARCHAR(50))
BEGIN
	INSERT INTO ACHIZITII_DETALII(Produsul,U_M,Cantitatea,Pretul_Unitar,Pret,Serie_Numar)
    VALUES(Produsul,U_M,Cantitatea,Pretul_Unitar,Pret,Serie_Numar);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Get_CUI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Get_CUI`()
BEGIN
	SELECT CUI FROM CLIENTI;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Insert_Client` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Insert_Client`(IN CUI VARCHAR(20),IN denumire VARCHAR(100),IN telefon VARCHAR(20),IN Client_Fidel VARCHAR(20),
								IN adresa VARCHAR(100),IN IBAN VARCHAR(20))
BEGIN
	INSERT INTO CLIENTI(CUI,Denumire,Telefon,Client_Fidel,Adresa,IBAN) VALUES(CUI,Denumire,Telefon,Client_Fidel,Adresa,IBAN);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Insert_Contract` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Insert_Contract`(IN Data_Incheierii VARCHAR(20),IN Avans VARCHAR(20),IN Avans_Platit VARCHAR(20),IN Rest_Plata VARCHAR(20),
							IN Contract_Platit VARCHAR(20),IN Data_Plata_Avans VARCHAR(20),IN Data_Plata_Rest VARCHAR(20),IN Cod_Cl VARCHAR(20))
BEGIN
	INSERT INTO CONTRACTE(Data_Incheierii,Avans,Avans_Platit,Rest_Plata,Contract_Platit,Data_Plata_Avans,Data_Plata_Rest,Cod_Client)
    VALUES(Data_Incheierii,Avans,Avans_Platit,Rest_Plata,Contract_Platit,Data_Plata_Avans,Data_Plata_Rest,
    (SELECT Cod_Client FROM CLIENTI WHERE Denumire = Cod_Cl));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `I_Facturi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `I_Facturi`(IN serie_numar VARCHAR(20),IN valoare_achizitie VARCHAR(20),IN penalizari VARCHAR(20),
	IN tip_factura VARCHAR(20),IN achitat VARCHAR(20),IN data_s VARCHAR(20),IN nr_contract_client VARCHAR(20),IN tip_insert VARCHAR(20))
BEGIN
	IF (tip_insert <> "NR. CONTRACT") THEN
		INSERT INTO FACTURI VALUES(serie_numar,valoare_achizitie,penalizari,tip_factura,achitat, data_s, NULL, nr_contract_client);
	ELSE
		INSERT INTO FACTURI VALUES(serie_numar,valoare_achizitie,penalizari,tip_factura,achitat, data_s,nr_contract_client ,
        (SELECT Cod_Client FROM contracte c WHERE c.Nr_Contract = nr_contract_client));
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `I_Facturi_Detalii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `I_Facturi_Detalii`(IN Nr_Crt VARCHAR(20),IN Denumire VARCHAR(20),IN Cantitate_Ore VARCHAR(20),IN Pretul_Unitar VARCHAR(20),
							IN Comentarii VARCHAR(300),IN Serie_Numar VARCHAR(20),IN Id_Serviciu VARCHAR(20))
BEGIN
INSERT INTO FACTURI_DETALII(Nr_Crt,Denumire,Cantitate_Ore,Pretul_Unitar,Comentarii,Serie_Numar,Id_Serviciu)
		VALUES(Nr_Crt,Denumire,Cantitate_Ore,Pretul_Unitar,Comentarii,Serie_Numar,Id_Serviciu);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `I_Testari` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `I_Testari`(IN Cod_Testare VARCHAR(20),IN Data_Test VARCHAR(20),IN Nr_Contract VARCHAR(20))
BEGIN
	INSERT INTO TESTARI(Cod_Testare,Data_Test,Nr_Contract) VALUES(Cod_Testare,Data_Test,Nr_Contract);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `I_Testari_Detalii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `I_Testari_Detalii`(IN Procedura VARCHAR(20),
					IN Pass VARCHAR(20),IN Rezultatul_Asteptat VARCHAR(20),IN Cod_Testare VARCHAR(20))
BEGIN
	INSERT INTO TESTARI_DETALII(Procedura,Rezultatul_Asteptat,Pass,Cod_Testare)
                              VALUES(Procedura,Rezultatul_Asteptat,Pass,Cod_Testare);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `I_User` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `I_User`(IN Username VARCHAR(30),IN Parola VARCHAR(50),IN Nume VARCHAR(50),IN Email VARCHAR(60),IN Nivel_Acces VARCHAR(1))
BEGIN
	INSERT INTO USERS(Username,Parola,Nume,Email,Nivel_Acces) VALUES(Username,Parola,Nume,Email,Nivel_Acces);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Login_Verif_Users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Login_Verif_Users`(IN utilizator VARCHAR(30),IN par VARCHAR(50))
BEGIN
	SELECT * FROM USERS WHERE Username = utilizator AND Parola = par;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Max_Index` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Max_Index`(IN Tabela VARCHAR(20))
BEGIN
	SET @sql = CONCAT("SELECT MAX(CAST(SUBSTRING_INDEX(serie_numar,'/',-1) AS UNSIGNED )) FROM ",Tabela);
    PREPARE s1 from @sql;EXECUTE s1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Max_Index_Testari` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Max_Index_Testari`()
BEGIN
	SELECT MAX(Cod_Testare) FROM TESTARI;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Stergere_Utilizator` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Stergere_Utilizator`(IN id VARCHAR(30))
BEGIN
	DELETE FROM USERS WHERE Cod_Utilizator = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Achizitie` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Achizitie`(IN id VARCHAR(20))
BEGIN
	SELECT *  FROM Achizitii_DETALII WHERE Serie_Numar = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Achizitii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Achizitii`(IN search_item VARCHAR(40),IN d_inceput VARCHAR(40),IN d_final VARCHAR(40),IN s_param VARCHAR(20))
BEGIN
	IF (s_param <> "") THEN
		SET @s=CONCAT('SELECT a.*,cl.Denumire FROM ACHIZITII a INNER JOIN CONTRACTE c ON c.Nr_Contract = a.Nr_Contract
		INNER JOIN CLIENTI cl ON c.Cod_Client = cl.Cod_Client WHERE ',search_item,' LIKE ',CONCAT('"%',s_param,'%"'),
        'AND Data_Achizitie Between "',d_inceput,'" and "',d_final ,'";');
        PREPARE stmt1 FROM @s;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;
    ELSE
		SELECT a.*,cl.Denumire FROM ACHIZITII a INNER JOIN CONTRACTE c ON c.Nr_Contract = a.Nr_Contract 
		INNER JOIN CLIENTI cl ON c.Cod_Client = cl.Cod_Client WHERE Data_Achizitie Between d_inceput and d_final;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Clienti` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Clienti`(IN s_param VARCHAR(20))
BEGIN
	IF (s_param <> "") THEN
		SELECT * FROM CLIENTI WHERE CUI LIke CONCAT('%',s_param,'%');
    ELSE
		SELECT * FROM CLIENTI;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Client_Like` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Client_Like`(IN den VARCHAR(20))
BEGIN
	SELECT Cod_Client,Denumire FROM CLIENTI WHERE( Denumire LIKE CONCAT('%',den,'%'));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Contracte` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Contracte`(IN search_item VARCHAR(40),IN d_inceput VARCHAR(40),IN d_final VARCHAR(40),IN s_param VARCHAR(20))
BEGIN
	IF (s_param <> "") THEN
		SET @s=CONCAT('SELECT c.*,cl.Denumire FROM Contracte c INNER JOIN CLIENTI cl ON c.Cod_Client = cl.Cod_Client 
        WHERE ',search_item,' LIKE ',CONCAT('"%',s_param,'%"'),' AND Data_Incheierii Between "',d_inceput,'" and "',d_final,'";');
        PREPARE stmt1 FROM @s;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;
    ELSE
		SELECT c.*,cl.Denumire FROM Contracte c INNER JOIN CLIENTI cl ON c.Cod_Client = cl.Cod_Client 
        WHERE Data_Incheierii Between d_inceput and d_final;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Facturi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Facturi`(IN search_item VARCHAR(40),IN d_inceput VARCHAR(40),IN d_final VARCHAR(40),IN s_param VARCHAR(20))
BEGIN
	IF (s_param <> "") THEN
		SET @s=CONCAT('SELECT f.*,cl.Denumire FROM FACTURI f INNER JOIN CLIENTI cl ON f.Cod_Client = cl.Cod_Client 
        WHERE ',search_item,' LIKE ',CONCAT('"%',s_param,'%"'),' AND Data_Scadenta Between "',d_inceput,'" and "',d_final,'";');
        PREPARE stmt1 FROM @s;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;
    ELSE
		SELECT f.*,cl.Denumire FROM FACTURI f INNER JOIN CLIENTI cl ON f.Cod_Client = cl.Cod_Client 
        WHERE Data_Scadenta Between d_inceput and d_final;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Facturi_D_Prod` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Facturi_D_Prod`(IN id VARCHAR(20))
BEGIN
	SELECT fd.*,d.Denumire as 'Serviciu' FROM FACTURI_DETALII fd LEFT JOIN DICTIONAR_SERVICII d ON fd.Id_Serviciu= d.Id_Serviciu 
    WHERE fd.Serie_Numar = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Facturi_Stornare` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Facturi_Stornare`(IN id VARCHAR(20))
BEGIN
	SELECT f.*,c.Denumire  FROM FACTURI f LEFT JOIN CLIENTI c ON c.Cod_Client = f.Cod_Client WHERE Serie_Numar = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Servicii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Servicii`()
BEGIN
	SELECT Id_Serviciu,Denumire,Pretul_Ora,Pretul_Ora_CFidel FROM DICTIONAR_SERVICII;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Testari` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Testari`(IN search_item VARCHAR(40),IN d_inceput VARCHAR(40),IN d_final VARCHAR(40),IN s_param VARCHAR(20))
BEGIN
	IF (s_param <> "") THEN
		SET @s=CONCAT('SELECT t.*,cl.Denumire FROM Testari t INNER JOIN CONTRACTE c ON t.Nr_Contract = c.Nr_Contract
        INNER JOIN CLIENTI cl ON cl.Cod_Client = c.Cod_Client
        WHERE ',search_item,' LIKE ',CONCAT('"%',s_param,'%"'),' AND Data_Test Between "',d_inceput,'" and "',d_final,'";');
        PREPARE stmt1 FROM @s;
		EXECUTE stmt1;
		DEALLOCATE PREPARE stmt1;
    ELSE
		SELECT t.*,cl.Denumire FROM Testari t INNER JOIN CONTRACTE c ON t.Nr_Contract = c.Nr_Contract
        INNER JOIN CLIENTI cl ON cl.Cod_Client = c.Cod_Client
        WHERE Data_Test Between d_inceput and d_final;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Testari_Detalii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Testari_Detalii`(IN id VARCHAR(20))
BEGIN
	SELECT * FROM Testari_DETALII WHERE Cod_Testare = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `S_Users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Users`(IN id VARCHAR(30))
BEGIN
	IF (id = "") THEN
		SELECT * FROM USERS;
    ELSE
		SELECT * FROM USERS WHERE Cod_Utilizator = id;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_Achizitii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_Achizitii`(IN suma VARCHAR(20),IN id VARCHAR(20))
BEGIN
	UPDATE Achizitii SET Valoare_Achizitie= suma WHERE Serie_Numar= id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_Achizitii_Detalii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_Achizitii_Detalii`(IN Produsul VARCHAR(20),IN U_M VARCHAR(20),
		IN Cantitatea VARCHAR(20),IN Pretul_Unitar VARCHAR(20),IN Pret VARCHAR(20),IN Cod_Det VARCHAR(20))
BEGIN
	UPDATE Achizitii_Detalii SET Produsul=Produsul,U_M=U_M,Cantitatea=Cantitatea,Pretul_Unitar = Pretul_Unitar,
		Pret=Pret WHERE Cod_Detalii = Cod_Det;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_Clienti` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_Clienti`(IN CUI VARCHAR(20),IN Denumire VARCHAR(70),IN Telefon VARCHAR(20),
			IN Client_Fidel VARCHAR(20),IN Adresa VARCHAR(100),IN IBAN VARCHAR(34),IN id VARCHAR(34))
BEGIN
	UPDATE CLIENTI SET CUI=CUI,Denumire=Denumire,Telefon=Telefon,
                    Client_Fidel=Client_Fidel,Adresa=Adresa,IBAN=IBAN WHERE Cod_Client= id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_Contracte` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_Contracte`(IN Avans_Platit VARCHAR(20),IN Contract_Platit VARCHAR(20),IN id VARCHAR(20))
BEGIN
	UPDATE CONTRACTE SET Avans_Platit=Avans_Platit, Contract_Platit=Contract_Platit  WHERE Nr_Contract= id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_Facturi_Detalii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_Facturi_Detalii`(IN Comentarii VARCHAR(300), IN id VARCHAR(20))
BEGIN
	UPDATE Facturi_Detalii SET Comentarii= Comentarii WHERE Cod_Detalii = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_F_Achitat` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_F_Achitat`(IN id VARCHAR(20), IN val VARCHAR(20))
BEGIN
	UPDATE Facturi SET Achitat= val WHERE Serie_Numar = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_Testari_Detalii` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_Testari_Detalii`(IN Procedura VARCHAR(90),IN Rezultatul_Asteptat VARCHAR(90),IN Pass VARCHAR(90),
			IN id VARCHAR(20))
BEGIN
	UPDATE TESTARI_Detalii SET Procedura=Procedura, Rezultatul_Asteptat= Rezultatul_Asteptat, Pass=Pass
			 WHERE Cod_Detalii = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `U_Users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `U_Users`(IN Username VARCHAR(30),IN Parola VARCHAR(50),IN Nume VARCHAR(50),
			IN Email VARCHAR(60),IN Nivel_Acces VARCHAR(1),IN id VARCHAR(30))
BEGIN
	UPDATE USERS SET Username=Username,Parola=Parola,Nume=Nume,Email=Email,Nivel_Acces=Nivel_Acces WHERE Cod_Utilizator = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Verfi_C_Fidel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Verfi_C_Fidel`(IN nr_contract_clienti VARCHAR(20),IN tip_insert VARCHAR(20))
BEGIN
	IF (tip_insert = "NR. CONTRACT") THEN
		SELECT Client_Fidel FROM CLIENTI cl INNER JOIN CONTRACTE c ON cl.Cod_Client = c.Cod_Client 
                     WHERE c.Nr_Contract = nr_contract_clienti ;
	ELSE
        SELECT Client_Fidel FROM CLIENTI cl  WHERE cl.Cod_Client = nr_contract_clienti;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Verif_Contract` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Verif_Contract`(IN nr_Contr VARCHAR(30))
BEGIN
	SELECT Nr_Contract FROM CONTRACTE WHERE Nr_Contract = nr_Contr;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-14  1:54:20
