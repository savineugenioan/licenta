﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Office.Interop.Excel;
using MySql.Data.MySqlClient;
using DataTable = System.Data.DataTable;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp1
{
    public partial class R_FN_F_Intarzieri : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        DataTable table;
        public R_FN_F_Intarzieri()
        {
            InitializeComponent();
        }
        public R_FN_F_Intarzieri(MySqlConnection con) : this()
        {
            this.con = con;
        }

        private void R_FN_F_Intarzieri_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            search.SelectedItem = "DENUMIRE CLIENT";
            d_inceput.Format = DateTimePickerFormat.Custom;
            d_inceput.CustomFormat = "yyyy-MM-dd";
            d_inceput.Value = DateTime.Today.AddDays(1);
            d_final.Format = DateTimePickerFormat.Custom;
            d_final.CustomFormat = "yyyy-MM-dd";
            d_final.Value = DateTime.Today.AddMonths(3);

            try
            {
                populate_grid();
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void search_SelectedIndexChanged(object sender, EventArgs e)
        {
            client.Text = "";
        }

        private void client_TextChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void d_inceput_ValueChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void d_final_ValueChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            string path;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }

            try
            {
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                xlWorkSheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;
                xlWorkSheet.get_Range("A1", "A1").Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWorkSheet.get_Range("A1", "A1").Style.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlWorkSheet.get_Range("a1", "h2").Merge(false);
                xlWorkSheet.Cells[1, 1] = "Facturi NeIncasate fara Intarzieri";
                xlWorkSheet.Cells[1, 1].Font.Size = 20;
                xlWorkSheet.Cells[1, 1].Font.Bold = true;
                xlWorkSheet.Cells[4, 1] = "Serie/Numar";
                xlWorkSheet.Cells[4, 2] = "Total";
                xlWorkSheet.Cells[4, 3] = "Penalizari";
                xlWorkSheet.Cells[4, 4] = "Tipul Facturii";
                xlWorkSheet.Cells[4, 5] = "Achitat";
                xlWorkSheet.Cells[4, 6] = "Data Scadenta";
                xlWorkSheet.Cells[4, 7] = "Nr. Contract";
                xlWorkSheet.Cells[4, 8] = "Client";
                var chartRange = xlWorkSheet.get_Range("a4", "h4");
                chartRange.Columns.AutoFit();
                xlWorkSheet.Columns[8].ColumnWidth = 40;//client


                int i = 5;
                int str = i;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    //populare tabel
                    xlWorkSheet.Cells[i, 1] = row.Cells[0].Value;
                    xlWorkSheet.Cells[i, 2] = row.Cells[1].Value;
                    xlWorkSheet.Cells[i, 3] = row.Cells[2].Value;
                    xlWorkSheet.Cells[i, 4] = row.Cells[3].Value;
                    xlWorkSheet.Cells[i, 5] = row.Cells[4].Value;
                    xlWorkSheet.Cells[i, 6] = row.Cells[5].Value;
                    xlWorkSheet.Cells[i, 7] = row.Cells[6].Value;
                    xlWorkSheet.Cells[i, 8] = row.Cells[8].Value;

                    i++;
                }


                //formatari tabel
                Range rng = xlWorkSheet.get_Range("A4", $"H{i - 1}");
                rng.WrapText = true;
                rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //adaugam perioada
                Range line = (Range)xlWorkSheet.Rows[3];
                line.Insert();
                xlWorkSheet.get_Range("a3", "h3").Merge(false);
                xlWorkSheet.Cells[3, 1] = $"Perioada : {d_inceput.Text} -> {d_final.Text}";

                //pt print repeat
                xlWorkSheet.PageSetup.PrintTitleRows = "$1:$5";

                //salvare fisier
                xlWorkBook.SaveAs($"{path}\\Facturi_NeIncasate_F_Intarzieri.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                MessageBox.Show($"Fisier XL creat : {path}\\Facturi_NeIncasate_F_Intarzieri.xls");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Cannot access"))
                {
                    MessageBox.Show($"Fisierul XL nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                        $"Facturi_NeIncasate_F_Intarzieri.xls si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //path XL
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            string path;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }

            //lista sortata dupa clienti
            var results = from p in table.AsEnumerable()
                          group p by p.Field<string>("Denumire") into g
                          select new { g.Key, Cars = g.ToList() };
            try
            {
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                //xlWorkSheet.PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape;
                xlWorkSheet.get_Range("A1", "A1").Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWorkSheet.get_Range("A1", "A1").Style.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlWorkSheet.get_Range("a1", "g2").Merge(false);
                xlWorkSheet.Cells[1, 1] = "Facturi NeIncasate fara Intarzieri pe Clienti";
                xlWorkSheet.Cells[1, 1].Font.Size = 20;
                xlWorkSheet.Cells[1, 1].Font.Bold = true;

                //adaugam perioada
                Range line = (Range)xlWorkSheet.Rows[3];
                line.Insert();
                xlWorkSheet.get_Range("a3", "g3").Merge(false);
                xlWorkSheet.Cells[3, 1] = $"Perioada : {d_inceput.Text} -> {d_final.Text}";

                int cur_row_xl = 4;
                results.ToList().ForEach(cl =>
                {
                    cur_row_xl++;
                    xlWorkSheet.get_Range($"a{cur_row_xl}", $"g{cur_row_xl}").Merge(false);
                    xlWorkSheet.get_Range($"a{cur_row_xl}", $"g{cur_row_xl}").Font.Bold = true;
                    xlWorkSheet.Cells[cur_row_xl, 1] = $"Clientul: {cl.Key}";
                    cur_row_xl++;

                    int start_pos_tabel = cur_row_xl;
                    xlWorkSheet.Cells[cur_row_xl, 1] = "Serie/Numar";
                    xlWorkSheet.Cells[cur_row_xl, 2] = "Total";
                    xlWorkSheet.Cells[cur_row_xl, 3] = "Penalizari";
                    xlWorkSheet.Cells[cur_row_xl, 4] = "Tipul Facturii";
                    xlWorkSheet.Cells[cur_row_xl, 5] = "Achitat";
                    xlWorkSheet.Cells[cur_row_xl, 6] = "Data Scadenta";
                    xlWorkSheet.Cells[cur_row_xl, 7] = "Nr. Contract";
                    xlWorkSheet.get_Range($"a{cur_row_xl}", $"g{cur_row_xl}").Columns.AutoFit();

                    cur_row_xl++;

                    cl.Cars.ForEach(col =>
                    {
                        var w = col.ItemArray;
                        for (int i = 0; i < w.Length - 2; i++)// fara 7 si 8 e clientul
                        {

                            if (i == 5)
                            {
                                string dt = DateTime.Parse(w[i].ToString()).ToString("yyyy-MM-dd");
                                xlWorkSheet.Cells[cur_row_xl, i + 1] = dt;
                            }
                            else
                            {
                                xlWorkSheet.Cells[cur_row_xl, i + 1] = w[i].ToString();
                            }
                        }
                        cur_row_xl++;
                    });
                    int stop_pos_tabel = cur_row_xl;

                    //formatari tabel
                    Range rng = xlWorkSheet.get_Range($"A{start_pos_tabel}", $"g{stop_pos_tabel - 1}");
                    rng.WrapText = true;
                    rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                });

                //pt print repeat
                xlWorkSheet.PageSetup.PrintTitleRows = "$1:$2";

                //salvare fisier
                xlWorkBook.SaveAs($"{path}\\Facturi_NeIncasate_F_Intarzieri_pe_Clienti.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                MessageBox.Show($"Fisier XL creat : {path}\\Facturi_NeIncasate_F_Intarzieri_pe_Clienti.xls");

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Cannot access"))
                {
                    MessageBox.Show($"Fisierul XL nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                        $"Facturi_Incasate_pe_Clienti.xls si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void populate_grid()
        {
            string search_item;
            if (search.SelectedItem.ToString() == "DENUMIRE CLIENT")
            {
                search_item = "cl.Denumire";
            }
            else
            {
                search_item = "f.Nr_Contract";
            }

            table = new DataTable();
            cmd = new MySqlCommand("S_Facturi", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@search_item", search_item);
            cmd.Parameters.AddWithValue("@d_inceput", d_inceput.Text);
            cmd.Parameters.AddWithValue("@d_final", d_final.Text);
            cmd.Parameters.AddWithValue("@s_param", client.Text.Trim());
            table.Load(cmd.ExecuteReader());
            var results = (from myRow in table.AsEnumerable()
                           where myRow.Field<int>("Achitat") == 0
                           select myRow).ToList();
            if (results.Count() > 0)
            {
                table = results.CopyToDataTable<DataRow>();
                this.dataGridView1.DataSource = table;

                dataGridView1.Columns[1].HeaderText = "Total Euro";
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    double total = double.Parse(row.Cells[1].Value.ToString());
                    row.Cells[1].Value = Math.Round(total);
                }
            }
            else
            {
                table.Rows.Clear();
                this.dataGridView1.DataSource = table;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string path;
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }
            try
            {
                if (File.Exists($"{ path}\\Facturi_NeIncasate_fara_Intarzieri.pdf"))
                {
                    DialogResult result = MessageBox.Show("Fisierul deja exista. Doriti sa il rescrieti?", "Confirmare", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (result == DialogResult.No)
                    {
                        return;
                    }
                }
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);
                FileStream fs = new System.IO.FileStream($"{path}\\Facturi_NeIncasate_fara_Intarzieri.pdf", System.IO.FileMode.Create);

                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                document.SetPageSize(PageSize.A4.Rotate());

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                // Open the document to enable you to write to the document  
                document.Open();
                // Add a simple and wellknown phrase to the document in a flow layout manner  
                Paragraph x = new Paragraph("Facturi NeIncasate fara Intarzieri", boldFont);
                x.Alignment = Element.ALIGN_CENTER;
                document.Add(x);
                x = new Paragraph($"Perioada: { d_inceput.Text }-> { d_final.Text}");
                x.Alignment = Element.ALIGN_CENTER;
                document.Add(x);
                document.Add(new Paragraph(" "));

                PdfPTable table1 = new PdfPTable(8);
                table1.HeaderRows = 1;

                table1.AddCell("Serie/Numar");
                table1.AddCell("Total");
                table1.AddCell("Penalizari");
                table1.AddCell("Tipul Facturii");
                table1.AddCell("Achitat");
                table1.AddCell("Data Scadenta");
                table1.AddCell("Nr. Contract");
                table1.AddCell("Client");

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    //populare tabel
                    table1.AddCell(row.Cells[0].Value.ToString());
                    table1.AddCell(row.Cells[1].Value.ToString());
                    table1.AddCell(row.Cells[2].Value.ToString());
                    table1.AddCell(row.Cells[3].Value.ToString());
                    table1.AddCell(row.Cells[4].Value.ToString());
                    table1.AddCell(row.Cells[5].Value.ToString());
                    table1.AddCell(row.Cells[6].Value.ToString());
                    table1.AddCell(row.Cells[8].Value.ToString());
                }

                document.Add(table1);
                document.Close();
                writer.Close();
                fs.Close();
                MessageBox.Show($"Fisier PDF creat : {path}\\Facturi_NeIncasate_fara_Intarzieri.pdf");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("cannot access"))
                {
                    MessageBox.Show($"Fisierul PDF nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                        $"Facturi_NeIncasate_fara_Intarzieri.pdf si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string path;
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }
            try
            {
                var results = from p in table.AsEnumerable()
                              group p by p.Field<string>("Denumire") into g
                              select new { g.Key, Cars = g.ToList() };
                if (File.Exists($"{ path}\\Facturi_NeIncasate_F_Intarzieri_pe_Clienti.pdf"))
                {
                    DialogResult result = MessageBox.Show("Fisierul deja exista. Doriti sa il rescrieti?", "Confirmare", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (result == DialogResult.No)
                    {
                        return;
                    }
                }
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);
                FileStream fs = new System.IO.FileStream($"{path}\\Facturi_NeIncasate_F_Intarzieri_pe_Clienti.pdf", System.IO.FileMode.Create);

                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                document.SetPageSize(PageSize.A4.Rotate());

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                // Open the document to enable you to write to the document  
                document.Open();
                // Add a simple and wellknown phrase to the document in a flow layout manner  
                Paragraph x = new Paragraph("Facturi NeIncasate fara Intarzieri pe clienti", boldFont);
                x.Alignment = Element.ALIGN_CENTER;
                document.Add(x);
                x = new Paragraph($"Perioada: { d_inceput.Text }-> { d_final.Text}");
                x.Alignment = Element.ALIGN_CENTER;
                document.Add(x);
                document.Add(new Paragraph(" "));

                results.ToList().ForEach(cl =>
                {

                    document.Add(new Paragraph($"Clientul: {cl.Key}"));
                    document.Add(new Paragraph(" "));

                    PdfPTable table1 = new PdfPTable(7);
                    table1.HeaderRows = 1;

                    table1.AddCell("Serie/Numar");
                    table1.AddCell("Total");
                    table1.AddCell("Penalizari");
                    table1.AddCell("Tipul Facturii");
                    table1.AddCell("Achitat");
                    table1.AddCell("Data Scadenta");
                    table1.AddCell("Nr. Contract");

                    cl.Cars.ForEach(col =>
                    {
                        var w = col.ItemArray;
                        for (int i = 0; i < w.Length - 2; i++)// fara 7 si 8 e clientul
                        {

                            if (i == 5)
                            {
                                string dt = DateTime.Parse(w[i].ToString()).ToString("yyyy-MM-dd");
                                table1.AddCell(dt);
                            }
                            else
                            {
                                table1.AddCell(w[i].ToString());
                            }
                        }
                    });
                    document.Add(table1);
                });
                document.Close();
                writer.Close();
                fs.Close();
                MessageBox.Show($"Fisier PDF creat : {path}\\Facturi_NeIncasate_F_Intarzieri_pe_Clienti.pdf");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("cannot access"))
                {
                    MessageBox.Show($"Fisierul PDF nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                        $"Facturi_NeIncasate_F_Intarzieri_pe_Clienti.pdf si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
