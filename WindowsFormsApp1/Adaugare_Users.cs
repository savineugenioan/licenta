﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Text;

namespace WindowsFormsApp1
{
    public partial class Adaugare_Users : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        public Adaugare_Users()
        {
            InitializeComponent();
        }
        public Adaugare_Users(MySqlConnection con) :this()
        {
            this.con = con;
        }

        private void A_Users_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MD5 hash = MD5.Create();
            string pass = GetMd5Hash(hash, textBox2.Text.Trim());
            // Introducere User
            cmd = new MySqlCommand("I_User", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Username", textBox1.Text.Trim());
            cmd.Parameters.AddWithValue("@Parola", pass);
            cmd.Parameters.AddWithValue("@Nume", textBox3.Text.Trim());
            cmd.Parameters.AddWithValue("@Email", textBox4.Text.Trim());
            cmd.Parameters.AddWithValue("@Nivel_Acces", comboBox1.Text.Trim());

            try
            {
                if (cmd.ExecuteNonQuery() == 1)
                    MessageBox.Show("Userul a fost introdus cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                else
                {
                    MessageBox.Show("A aparut o eroare la introducerea datelor", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Close();
            }
            catch(MySqlException err)
            {
                if(err.Message.Contains("Username"))
                    MessageBox.Show("Username Existent", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (err.Message.Contains("Email"))
                    MessageBox.Show("Email Existent", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);//err
                return;
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
