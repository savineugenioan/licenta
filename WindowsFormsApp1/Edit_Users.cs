﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Edit_Users : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        string id;
        public Edit_Users()
        {
            InitializeComponent();
        }
        public Edit_Users(MySqlConnection con, string id) : this()
        {
            this.con = con;
            this.id = id;
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private void Edit_Users_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            cmd = new MySqlCommand("S_Users", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            rdr = cmd.ExecuteReader();
            rdr.Read();
            textBox1.Text = rdr[1].ToString();
            textBox2.Text = rdr[2].ToString();
            textBox3.Text = rdr[3].ToString();
            textBox4.Text = rdr[4].ToString();
            comboBox1.SelectedItem = rdr[5].ToString();
            rdr.Close();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            MD5 hash = MD5.Create();
            string pass = GetMd5Hash(hash, textBox2.Text.Trim());
            cmd = new MySqlCommand("U_Users", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Username", textBox1.Text.Trim());
            cmd.Parameters.AddWithValue("@Parola", pass.Trim());
            cmd.Parameters.AddWithValue("@Nume", textBox3.Text.Trim());
            cmd.Parameters.AddWithValue("@Email", textBox4.Text.Trim());
            cmd.Parameters.AddWithValue("@Nivel_Acces", comboBox1.Text.Trim());
            cmd.Parameters.AddWithValue("@id", id);

            try
            {
                cmd.ExecuteNonQuery();

                MessageBox.Show("Userul a fost introdus cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            catch (MySqlException err)
            {
                if (err.Message.Contains("Username"))
                    MessageBox.Show("Username Existent", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (err.Message.Contains("Email"))
                    MessageBox.Show("Email Existent", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
