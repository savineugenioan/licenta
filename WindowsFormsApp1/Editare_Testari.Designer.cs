﻿namespace WindowsFormsApp1
{
    partial class Editare_Testari
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.search = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.d_final = new System.Windows.Forms.DateTimePicker();
            this.d_inceput = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.client = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(25, 57);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(568, 260);
            this.dataGridView1.TabIndex = 13;
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(157, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = ":";
            // 
            // search
            // 
            this.search.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.search.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.Items.AddRange(new object[] {
            "DENUMIRE CLIENT",
            "NR. CONTRACT"});
            this.search.Location = new System.Drawing.Point(25, 22);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(128, 21);
            this.search.TabIndex = 36;
            this.search.SelectedIndexChanged += new System.EventHandler(this.search_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(597, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 25);
            this.label4.TabIndex = 35;
            this.label4.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(401, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Perioada:";
            // 
            // d_final
            // 
            this.d_final.Location = new System.Drawing.Point(617, 23);
            this.d_final.Name = "d_final";
            this.d_final.Size = new System.Drawing.Size(134, 20);
            this.d_final.TabIndex = 33;
            this.d_final.ValueChanged += new System.EventHandler(this.d_final_ValueChanged);
            // 
            // d_inceput
            // 
            this.d_inceput.Location = new System.Drawing.Point(459, 23);
            this.d_inceput.Name = "d_inceput";
            this.d_inceput.Size = new System.Drawing.Size(134, 20);
            this.d_inceput.TabIndex = 32;
            this.d_inceput.ValueChanged += new System.EventHandler(this.d_inceput_ValueChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(604, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 38);
            this.label2.TabIndex = 31;
            this.label2.Text = "* Puteti selecta o intrare dand Dublu Click pe aceasta";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // client
            // 
            this.client.Location = new System.Drawing.Point(169, 23);
            this.client.Name = "client";
            this.client.Size = new System.Drawing.Size(185, 20);
            this.client.TabIndex = 30;
            this.client.TextChanged += new System.EventHandler(this.client_TextChanged);
            // 
            // Editare_Testari
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 333);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.search);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.d_final);
            this.Controls.Add(this.d_inceput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.client);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "Editare_Testari";
            this.Text = "Editare_Testari";
            this.Load += new System.EventHandler(this.Editare_Testari_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox search;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker d_final;
        private System.Windows.Forms.DateTimePicker d_inceput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox client;
    }
}