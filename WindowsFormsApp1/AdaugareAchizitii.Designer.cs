﻿namespace WindowsFormsApp1
{
    partial class AdaugareAchizitii
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.achizitii = new System.Windows.Forms.DataGridView();
            this.Produs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantitatea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.numar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.serie = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nr_contract = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.data_c = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.achizitii)).BeginInit();
            this.SuspendLayout();
            // 
            // achizitii
            // 
            this.achizitii.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.achizitii.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Produs,
            this.UM,
            this.Cantitatea,
            this.PU});
            this.achizitii.Location = new System.Drawing.Point(26, 283);
            this.achizitii.Name = "achizitii";
            this.achizitii.Size = new System.Drawing.Size(501, 150);
            this.achizitii.TabIndex = 62;
            this.achizitii.Visible = false;
            this.achizitii.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.achizitii_EditingControlShowing);
            // 
            // Produs
            // 
            this.Produs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Produs.HeaderText = "Produs";
            this.Produs.MinimumWidth = 150;
            this.Produs.Name = "Produs";
            // 
            // UM
            // 
            this.UM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.UM.HeaderText = "U. de Masura";
            this.UM.Name = "UM";
            this.UM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.UM.Width = 96;
            // 
            // Cantitatea
            // 
            this.Cantitatea.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Cantitatea.HeaderText = "Cantitatea";
            this.Cantitatea.Name = "Cantitatea";
            this.Cantitatea.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Cantitatea.Width = 80;
            // 
            // PU
            // 
            this.PU.HeaderText = "Pretul Unitar";
            this.PU.Name = "PU";
            this.PU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "Data";
            // 
            // numar
            // 
            this.numar.Location = new System.Drawing.Point(149, 86);
            this.numar.Name = "numar";
            this.numar.ReadOnly = true;
            this.numar.Size = new System.Drawing.Size(200, 20);
            this.numar.TabIndex = 54;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Numar";
            // 
            // serie
            // 
            this.serie.Location = new System.Drawing.Point(149, 49);
            this.serie.Name = "serie";
            this.serie.ReadOnly = true;
            this.serie.Size = new System.Drawing.Size(200, 20);
            this.serie.TabIndex = 52;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Serie";
            // 
            // nr_contract
            // 
            this.nr_contract.Location = new System.Drawing.Point(149, 235);
            this.nr_contract.Name = "nr_contract";
            this.nr_contract.Size = new System.Drawing.Size(200, 20);
            this.nr_contract.TabIndex = 64;
            this.nr_contract.TextChanged += new System.EventHandler(this.nr_contract_TextChanged);
            this.nr_contract.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nr_contract_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 63;
            this.label4.Text = "Nr Contract";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(26, 466);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 3, 30, 15);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(501, 35);
            this.button1.TabIndex = 65;
            this.button1.Text = "Adaugare Achizitie";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // data_c
            // 
            this.data_c.Location = new System.Drawing.Point(149, 161);
            this.data_c.Name = "data_c";
            this.data_c.Size = new System.Drawing.Size(200, 20);
            this.data_c.TabIndex = 66;
            // 
            // AdaugareAchizitii
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(573, 574);
            this.Controls.Add(this.data_c);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nr_contract);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.achizitii);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.serie);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "AdaugareAchizitii";
            this.Text = "Adaugare Achizitii";
            this.Load += new System.EventHandler(this.AdaugareAchizitii_Load);
            ((System.ComponentModel.ISupportInitialize)(this.achizitii)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView achizitii;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox numar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox serie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nr_contract;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produs;
        private System.Windows.Forms.DataGridViewTextBoxColumn UM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantitatea;
        private System.Windows.Forms.DataGridViewTextBoxColumn PU;
        private System.Windows.Forms.DateTimePicker data_c;
    }
}