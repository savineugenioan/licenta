﻿namespace WindowsFormsApp1
{
    partial class AdaugareContracte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.avans = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.text_clienti = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lista_clienti = new System.Windows.Forms.ListBox();
            this.avans_ck = new System.Windows.Forms.CheckBox();
            this.contract_ck = new System.Windows.Forms.CheckBox();
            this.adaugare_contract = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.data_incheiere = new System.Windows.Forms.DateTimePicker();
            this.data_avans = new System.Windows.Forms.DateTimePicker();
            this.data_lichidare = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Data Limita a platii avansului";
            // 
            // total
            // 
            this.total.Location = new System.Drawing.Point(181, 104);
            this.total.Multiline = true;
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(183, 21);
            this.total.TabIndex = 18;
            this.total.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.total_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Rest de Plata";
            // 
            // avans
            // 
            this.avans.Location = new System.Drawing.Point(181, 62);
            this.avans.Name = "avans";
            this.avans.Size = new System.Drawing.Size(183, 20);
            this.avans.TabIndex = 16;
            this.avans.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.avans_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Avans";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Data Incheierii";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Data Lichidarii";
            // 
            // text_clienti
            // 
            this.text_clienti.Location = new System.Drawing.Point(181, 206);
            this.text_clienti.Name = "text_clienti";
            this.text_clienti.Size = new System.Drawing.Size(183, 20);
            this.text_clienti.TabIndex = 27;
            this.text_clienti.TextChanged += new System.EventHandler(this.text_clienti_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Cautare Client";
            // 
            // lista_clienti
            // 
            this.lista_clienti.FormattingEnabled = true;
            this.lista_clienti.Location = new System.Drawing.Point(181, 245);
            this.lista_clienti.Name = "lista_clienti";
            this.lista_clienti.Size = new System.Drawing.Size(183, 121);
            this.lista_clienti.TabIndex = 29;
            // 
            // avans_ck
            // 
            this.avans_ck.AccessibleDescription = "";
            this.avans_ck.AutoSize = true;
            this.avans_ck.Location = new System.Drawing.Point(405, 64);
            this.avans_ck.Name = "avans_ck";
            this.avans_ck.Size = new System.Drawing.Size(82, 17);
            this.avans_ck.TabIndex = 30;
            this.avans_ck.Text = "Avans Platit";
            this.avans_ck.UseVisualStyleBackColor = true;
            // 
            // contract_ck
            // 
            this.contract_ck.AutoSize = true;
            this.contract_ck.Location = new System.Drawing.Point(404, 106);
            this.contract_ck.Name = "contract_ck";
            this.contract_ck.Size = new System.Drawing.Size(92, 17);
            this.contract_ck.TabIndex = 31;
            this.contract_ck.Text = "Contract Platit";
            this.contract_ck.UseVisualStyleBackColor = true;
            // 
            // adaugare_contract
            // 
            this.adaugare_contract.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adaugare_contract.Location = new System.Drawing.Point(27, 387);
            this.adaugare_contract.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.adaugare_contract.Name = "adaugare_contract";
            this.adaugare_contract.Size = new System.Drawing.Size(460, 35);
            this.adaugare_contract.TabIndex = 32;
            this.adaugare_contract.Text = "Adaugare Contract";
            this.adaugare_contract.UseVisualStyleBackColor = true;
            this.adaugare_contract.Click += new System.EventHandler(this.adaugare_contract_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(27, 334);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 32);
            this.label7.TabIndex = 59;
            this.label7.Text = "Clientul trebuie selectat din lista alaturata ->";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(27, 245);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(133, 75);
            this.button3.TabIndex = 58;
            this.button3.Text = "Adaugare Client Nou";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // data_incheiere
            // 
            this.data_incheiere.Location = new System.Drawing.Point(181, 25);
            this.data_incheiere.Name = "data_incheiere";
            this.data_incheiere.Size = new System.Drawing.Size(183, 20);
            this.data_incheiere.TabIndex = 60;
            // 
            // data_avans
            // 
            this.data_avans.Location = new System.Drawing.Point(181, 142);
            this.data_avans.Name = "data_avans";
            this.data_avans.Size = new System.Drawing.Size(183, 20);
            this.data_avans.TabIndex = 61;
            // 
            // data_lichidare
            // 
            this.data_lichidare.Location = new System.Drawing.Point(181, 180);
            this.data_lichidare.Name = "data_lichidare";
            this.data_lichidare.Size = new System.Drawing.Size(183, 20);
            this.data_lichidare.TabIndex = 62;
            // 
            // AdaugareContracte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(508, 434);
            this.Controls.Add(this.data_lichidare);
            this.Controls.Add(this.data_avans);
            this.Controls.Add(this.data_incheiere);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.adaugare_contract);
            this.Controls.Add(this.contract_ck);
            this.Controls.Add(this.avans_ck);
            this.Controls.Add(this.lista_clienti);
            this.Controls.Add(this.text_clienti);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.total);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.avans);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "AdaugareContracte";
            this.Text = "Adaugare Contracte";
            this.Load += new System.EventHandler(this.AdaugareContracte_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox avans;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox text_clienti;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lista_clienti;
        private System.Windows.Forms.CheckBox avans_ck;
        private System.Windows.Forms.CheckBox contract_ck;
        private System.Windows.Forms.Button adaugare_contract;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker data_incheiere;
        private System.Windows.Forms.DateTimePicker data_avans;
        private System.Windows.Forms.DateTimePicker data_lichidare;
    }
}