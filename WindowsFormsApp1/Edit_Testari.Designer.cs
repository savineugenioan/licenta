﻿namespace WindowsFormsApp1
{
    partial class Edit_Testari
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.testari = new System.Windows.Forms.DataGridView();
            this.Cod_Detalii = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Procedura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RezultatulAsteptat = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Pass = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Cod_Testare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.testari)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(335, 256);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(324, 35);
            this.button1.TabIndex = 58;
            this.button1.Text = "Editare Specificatie";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // testari
            // 
            this.testari.AllowUserToAddRows = false;
            this.testari.AllowUserToDeleteRows = false;
            this.testari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.testari.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cod_Detalii,
            this.Procedura,
            this.RezultatulAsteptat,
            this.Pass,
            this.Cod_Testare});
            this.testari.Location = new System.Drawing.Point(12, 26);
            this.testari.Name = "testari";
            this.testari.Size = new System.Drawing.Size(900, 224);
            this.testari.TabIndex = 73;
            // 
            // Cod_Detalii
            // 
            this.Cod_Detalii.HeaderText = "Cod_Detalii";
            this.Cod_Detalii.Name = "Cod_Detalii";
            this.Cod_Detalii.ReadOnly = true;
            // 
            // Procedura
            // 
            this.Procedura.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Procedura.HeaderText = "Procedura";
            this.Procedura.Name = "Procedura";
            // 
            // RezultatulAsteptat
            // 
            dataGridViewCellStyle1.NullValue = "DA";
            this.RezultatulAsteptat.DefaultCellStyle = dataGridViewCellStyle1;
            this.RezultatulAsteptat.HeaderText = "Rezultatul Asteptat";
            this.RezultatulAsteptat.Items.AddRange(new object[] {
            "NU",
            "DA"});
            this.RezultatulAsteptat.Name = "RezultatulAsteptat";
            this.RezultatulAsteptat.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Pass
            // 
            this.Pass.FalseValue = "0";
            this.Pass.HeaderText = "Pass";
            this.Pass.Name = "Pass";
            this.Pass.TrueValue = "1";
            // 
            // Cod_Testare
            // 
            this.Cod_Testare.HeaderText = "Cod_Testare";
            this.Cod_Testare.Name = "Cod_Testare";
            this.Cod_Testare.ReadOnly = true;
            // 
            // Edit_Testari
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 315);
            this.Controls.Add(this.testari);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.Name = "Edit_Testari";
            this.Text = "Edit Testari";
            this.Load += new System.EventHandler(this.Edit_Testari_Load);
            ((System.ComponentModel.ISupportInitialize)(this.testari)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView testari;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cod_Detalii;
        private System.Windows.Forms.DataGridViewTextBoxColumn Procedura;
        private System.Windows.Forms.DataGridViewComboBoxColumn RezultatulAsteptat;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Pass;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cod_Testare;
    }
}