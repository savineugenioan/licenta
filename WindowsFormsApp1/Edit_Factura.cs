﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Edit_Factura : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        string id;
        public Edit_Factura()
        {
            InitializeComponent();
        }
        public Edit_Factura(MySqlConnection _con, DataGridViewRow row) : this()
        {
            this.con = _con;
            id = row.Cells[0].Value.ToString();
            serie_numar.Text = id;
        }

        private void Edit_Factura_Load(object sender, EventArgs e)
        {

            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            try
            {
                DataTable table = new DataTable();

                cmd = new MySqlCommand("S_Facturi_D_Prod", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                table.Load(cmd.ExecuteReader());
                this.dataGridView1.DataSource = table;
                dataGridView1.Columns[0].Width = 70;
                dataGridView1.Columns[0].ReadOnly = true;
                dataGridView1.Columns[1].ReadOnly = true;
                dataGridView1.Columns[2].ReadOnly = true;
                dataGridView1.Columns[3].ReadOnly = true;
                dataGridView1.Columns[4].ReadOnly = true;
                dataGridView1.Columns[6].ReadOnly = true;
                dataGridView1.Columns[7].ReadOnly = true;
                dataGridView1.Columns[8].ReadOnly = true;
                dataGridView1.Columns[1].Width = 60;
                dataGridView1.Columns[6].Visible = false;
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[5].ReadOnly = false;
                
                dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                foreach(DataGridViewRow row in dataGridView1.Rows)
                {
                    cmd = new MySqlCommand("U_Facturi_Detalii", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Comentarii", row.Cells[5].Value.ToString());
                    cmd.Parameters.AddWithValue("@id", row.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                }

                MessageBox.Show("Factura a fost editata cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
