﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Conexiune
    {
        private static  Configuration config ;

        public static void getConfiguration()
        {
            string server = Properties.Settings.Default.Server;
            string port = Properties.Settings.Default.Port;
            string DB = Properties.Settings.Default.DB;
            string user = Properties.Settings.Default.User;
            string parola = Properties.Settings.Default.Parola ;

            string cs = $"Server={server};Port={port};Database={DB};" +
                $"Uid={user};Pwd={parola};Connection Timeout=30";
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings[0].ConnectionString = cs;
            config.ConnectionStrings.ConnectionStrings[0].ProviderName = "System.Data.SqlClient";
            config.Save(ConfigurationSaveMode.Modified);
        }
        public static void SaveConnectionString(string value)
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings[0].ConnectionString = value;
            config.ConnectionStrings.ConnectionStrings[0].ProviderName = "System.Data.SqlClient";
            config.Save(ConfigurationSaveMode.Modified);
        }

        public static string GetConnectionString()
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            return config.ConnectionStrings.ConnectionStrings[0].ConnectionString;
        }
    }
}
