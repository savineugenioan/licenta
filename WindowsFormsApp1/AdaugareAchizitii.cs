﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class AdaugareAchizitii : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;

        public AdaugareAchizitii()
        {
            InitializeComponent();
        }
        public AdaugareAchizitii(MySqlConnection _con) : this()
        {
            con = _con;
        }

        private void AdaugareAchizitii_Load(object sender, EventArgs e)
        {
            data_c.Format = DateTimePickerFormat.Custom;
            data_c.CustomFormat = "yyyy-MM-dd";

            DateTime dateTime = DateTime.UtcNow.Date;
            serie.Text = dateTime.ToString("yyyy");

            dateTime = data_c.Value;
            try
            {


                if (con.State == ConnectionState.Open)
                {

                    // CALCULEZ SERIA SI NUMARUL
                    cmd = new MySqlCommand("Max_Index", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Tabela","achizitii");
                    int i = Int32.Parse(cmd.ExecuteScalar().ToString()) + 1;
                    numar.Text = i.ToString();

                }
                else
                {
                    MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //verificari de inceput
            if(Int32.TryParse(nr_contract.Text,out int res) == false)
            {
                MessageBox.Show("Introduceti Numarul Contractului  ( Corect! )", "Nr Contract", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (achizitii.Rows.Count == 1 && Globalz.EmptyCellsNoAdd(achizitii))
            {
                MessageBox.Show("Toate celulele trebuie sa contina date !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Globalz.EmptyCells(achizitii))
            {
                MessageBox.Show("Toate celulele trebuie sa contina date !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //calcul val_achizitie
            double valoare_achizitie = 0;
            var rows = achizitii.Rows;
            for (int j = 0; j < rows.Count - 1; j++)
            {
                string[] cells = new string[4];
                for (int i = 0; i < 4; i++)
                {
                    cells[i] = rows[j].Cells[i].Value.ToString();

                }
                valoare_achizitie += Int32.Parse(cells[2]) * Int32.Parse(cells[3]);
            }

           
            if (con.State == ConnectionState.Open)
            {
                try
                {
                    //Insert in Achizitii
                    cmd = new MySqlCommand("Adaugare_Achizitii", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@serie_numar", serie.Text + '/' + numar.Text);
                    cmd.Parameters.AddWithValue("@data_c", data_c.Text);
                    cmd.Parameters.AddWithValue("@valoare_achizitie", valoare_achizitie);
                    cmd.Parameters.AddWithValue("@nr_contract", nr_contract.Text);
                    int x = cmd.ExecuteNonQuery();

                    for (int j=0;j<rows.Count-1;j++)
                    {
                        string[] cells = new string[4];
                        for (int i = 0; i < 4; i++)
                        {
                            cells[i] = rows[j].Cells[i].Value.ToString();

                        }

                        //Insert in Achizitii_Detalii
                        cmd = new MySqlCommand("Adaugare_Achizitii_Detalii", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Produsul", cells[0]);
                        cmd.Parameters.AddWithValue("@U_M", cells[1]);
                        cmd.Parameters.AddWithValue("@Cantitatea", cells[2]);
                        cmd.Parameters.AddWithValue("@Pretul_Unitar", cells[3]);
                        cmd.Parameters.AddWithValue("@Pret", Int32.Parse(cells[2]) * Int32.Parse(cells[3]));
                        cmd.Parameters.AddWithValue("@Serie_Numar", serie.Text + '/' + numar.Text);
                        x = cmd.ExecuteNonQuery();
                        
                    }
                    MessageBox.Show("Datele au fost introduse cu succes !","SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                catch(Exception err)
                {
                    if (err.Message.Contains("Cannot add or update a child row: a foreign key constraint fails"))
                    {
                        MessageBox.Show($"Contractul nu exista in baza de date ! ", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                }
            }
            else
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        // EVENT declansat cand scriu in CELL in GRID
        private void achizitii_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Not_Number_Press);
            if (achizitii.CurrentCell.ColumnIndex == 2 || achizitii.CurrentCell.ColumnIndex == 3)
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Not_Number_Press);
                }
            }
        }

        // EVENT verific daca e numar
        private void Not_Number_Press(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void nr_contract_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void nr_contract_TextChanged(object sender, EventArgs e)
        {
            if (nr_contract.Text == "")
                achizitii.Rows.Clear();
            if (Int32.TryParse(nr_contract.Text, out int res) == true)
                achizitii.Visible = true;
            else
                achizitii.Visible = false;
        }
    }
}
