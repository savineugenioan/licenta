﻿namespace WindowsFormsApp1
{
    partial class AdaugareTestat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.nr_contract = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.testari = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.numar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.data = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.Procedura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RezultatulAsteptat = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Pass = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.testari)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(30, 401);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(814, 35);
            this.button1.TabIndex = 75;
            this.button1.Text = "Adaugare Specificatie de Testare";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // nr_contract
            // 
            this.nr_contract.Location = new System.Drawing.Point(369, 174);
            this.nr_contract.Name = "nr_contract";
            this.nr_contract.Size = new System.Drawing.Size(200, 20);
            this.nr_contract.TabIndex = 74;
            this.nr_contract.TextChanged += new System.EventHandler(this.nr_contract_TextChanged);
            this.nr_contract.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nr_contract_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(279, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 73;
            this.label4.Text = "Nr. Contract";
            // 
            // testari
            // 
            this.testari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.testari.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Procedura,
            this.RezultatulAsteptat,
            this.Pass});
            this.testari.Location = new System.Drawing.Point(30, 217);
            this.testari.Name = "testari";
            this.testari.Size = new System.Drawing.Size(814, 150);
            this.testari.TabIndex = 72;
            this.testari.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 70;
            this.label3.Text = "Data";
            // 
            // numar
            // 
            this.numar.Location = new System.Drawing.Point(369, 25);
            this.numar.Name = "numar";
            this.numar.ReadOnly = true;
            this.numar.Size = new System.Drawing.Size(200, 20);
            this.numar.TabIndex = 69;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 68;
            this.label2.Text = "Nr. Testare";
            // 
            // data
            // 
            this.data.Location = new System.Drawing.Point(369, 100);
            this.data.Name = "data";
            this.data.Size = new System.Drawing.Size(200, 20);
            this.data.TabIndex = 76;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(587, 172);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 23);
            this.button2.TabIndex = 77;
            this.button2.Text = "Verificare Contract";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Procedura
            // 
            this.Procedura.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Procedura.HeaderText = "Procedura";
            this.Procedura.Name = "Procedura";
            // 
            // RezultatulAsteptat
            // 
            dataGridViewCellStyle1.NullValue = "DA";
            this.RezultatulAsteptat.DefaultCellStyle = dataGridViewCellStyle1;
            this.RezultatulAsteptat.HeaderText = "Rezultatul Asteptat";
            this.RezultatulAsteptat.Items.AddRange(new object[] {
            "NU",
            "DA"});
            this.RezultatulAsteptat.Name = "RezultatulAsteptat";
            this.RezultatulAsteptat.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Pass
            // 
            this.Pass.FalseValue = "0";
            this.Pass.HeaderText = "Pass";
            this.Pass.Name = "Pass";
            this.Pass.TrueValue = "1";
            // 
            // AdaugareTestat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 446);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.data);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nr_contract);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.testari);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numar);
            this.Controls.Add(this.label2);
            this.Name = "AdaugareTestat";
            this.Text = "Adaugare Specificatii De Testat";
            this.Load += new System.EventHandler(this.AdaugareTestat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.testari)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox nr_contract;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView testari;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox numar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker data;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Procedura;
        private System.Windows.Forms.DataGridViewComboBoxColumn RezultatulAsteptat;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Pass;
    }
}