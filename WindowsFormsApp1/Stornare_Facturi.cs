﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Stornare_Facturi : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        public Stornare_Facturi()
        {
            InitializeComponent();
        }
        public Stornare_Facturi(MySqlConnection _con) :this()
        {
            this.con = _con;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //populare Grid
            var x = serie_numar.Text;
            cmd = new MySqlCommand("S_Facturi_Stornare", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", x);
            string i = (string) cmd.ExecuteScalar();

            if (i == null)
            {
                textBox1.Text = "Serie/Numar introdus gresit !";
                textBox1.BackColor = Color.Red;
            }
            else
            {
                textBox1.Text = "Factura Gasita!";
                textBox1.BackColor = Color.Green;
                DataTable table = new DataTable();
                table.Load(cmd.ExecuteReader());
                this.dataGridView1.DataSource = table;
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {

            //INSERARE STORNARE + Achitare F Stornata
            try
            {
                if(dataGridView1.Rows.Count > 0)
                {
                    string nr_contract_client, tip_insert;
                    if(dataGridView1[6, 0].Value.ToString() != "")
                    {
                        tip_insert = "NR. CONTRACT";
                        nr_contract_client = dataGridView1[6, 0].Value.ToString();
                    }
                    else
                    {
                        tip_insert = "";
                        nr_contract_client = dataGridView1[7, 0].Value.ToString(); ;
                    }

                    string s_n = "S_" + dataGridView1[0, 0].Value.ToString();
                    DateTime d = DateTime.Parse(dataGridView1[5, 0].Value.ToString());

                    cmd = new MySqlCommand("I_Facturi", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@serie_numar", s_n);
                    cmd.Parameters.AddWithValue("@valoare_achizitie", dataGridView1[1, 0].Value.ToString());
                    cmd.Parameters.AddWithValue("@penalizari", dataGridView1[2, 0].Value.ToString());
                    cmd.Parameters.AddWithValue("@tip_factura", "STORNARE");
                    cmd.Parameters.AddWithValue("@achitat", "1");
                    cmd.Parameters.AddWithValue("@data_s", d.ToString("yyyy-MM-dd"));
                    cmd.Parameters.AddWithValue("@nr_contract_client", nr_contract_client);
                    cmd.Parameters.AddWithValue("@tip_insert", tip_insert);
                    cmd.ExecuteNonQuery();

                    cmd = new MySqlCommand("U_F_Achitat", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", dataGridView1[0, 0].Value.ToString());
                    cmd.Parameters.AddWithValue("@val", "1");
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Factura Stornata!", "OK");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Selectati o factura pentru stornare !", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch(Exception ex)
            {
                if(ex.Message.Contains("Duplicate entry"))
                {
                    MessageBox.Show("Factura este deja stornata", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Stornare_Facturi_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
        }
    }
}
