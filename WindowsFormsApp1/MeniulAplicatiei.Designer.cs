﻿namespace WindowsFormsApp1
{
    partial class MeniulAplicatiei
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.adaugareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLIENTIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cONTRACTEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fACTURIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aCHIZITIIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sPECIFICATIIDETESTATToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSERSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLIENTIToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cONTRACTEToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fACTURIToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sTORNAREFACTURAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aCHIZITIIToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sPECIFICATIIDETESTATToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uSERSToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rapoarteListariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fACTURIToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.lEIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eUROToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bORDEROUDEFACTURIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fACTURIINCASATEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fACTURINEPLATITEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cUINTARZIERIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fARAINTARZIERIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fACTURISTORNATEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rAPORTDEACHIZITIIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rAPORTDETESTAREToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.reconectare = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adaugareToolStripMenuItem,
            this.editareToolStripMenuItem,
            this.rapoarteListariToolStripMenuItem,
            this.setariToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(415, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // adaugareToolStripMenuItem
            // 
            this.adaugareToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cLIENTIToolStripMenuItem,
            this.cONTRACTEToolStripMenuItem,
            this.fACTURIToolStripMenuItem,
            this.aCHIZITIIToolStripMenuItem,
            this.sPECIFICATIIDETESTATToolStripMenuItem,
            this.uSERSToolStripMenuItem});
            this.adaugareToolStripMenuItem.Name = "adaugareToolStripMenuItem";
            this.adaugareToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.adaugareToolStripMenuItem.Text = "Adaugare";
            // 
            // cLIENTIToolStripMenuItem
            // 
            this.cLIENTIToolStripMenuItem.Name = "cLIENTIToolStripMenuItem";
            this.cLIENTIToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.cLIENTIToolStripMenuItem.Text = "CLIENTI";
            this.cLIENTIToolStripMenuItem.Click += new System.EventHandler(this.cLIENTIToolStripMenuItem_Click);
            // 
            // cONTRACTEToolStripMenuItem
            // 
            this.cONTRACTEToolStripMenuItem.Name = "cONTRACTEToolStripMenuItem";
            this.cONTRACTEToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.cONTRACTEToolStripMenuItem.Text = "CONTRACTE";
            this.cONTRACTEToolStripMenuItem.Click += new System.EventHandler(this.cONTRACTEToolStripMenuItem_Click);
            // 
            // fACTURIToolStripMenuItem
            // 
            this.fACTURIToolStripMenuItem.Name = "fACTURIToolStripMenuItem";
            this.fACTURIToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.fACTURIToolStripMenuItem.Text = "FACTURI";
            this.fACTURIToolStripMenuItem.Click += new System.EventHandler(this.fACTURIToolStripMenuItem_Click);
            // 
            // aCHIZITIIToolStripMenuItem
            // 
            this.aCHIZITIIToolStripMenuItem.Name = "aCHIZITIIToolStripMenuItem";
            this.aCHIZITIIToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.aCHIZITIIToolStripMenuItem.Text = "ACHIZITII";
            this.aCHIZITIIToolStripMenuItem.Click += new System.EventHandler(this.aCHIZITIIToolStripMenuItem_Click);
            // 
            // sPECIFICATIIDETESTATToolStripMenuItem
            // 
            this.sPECIFICATIIDETESTATToolStripMenuItem.Name = "sPECIFICATIIDETESTATToolStripMenuItem";
            this.sPECIFICATIIDETESTATToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.sPECIFICATIIDETESTATToolStripMenuItem.Text = "SPECIFICATII DE TESTAT";
            this.sPECIFICATIIDETESTATToolStripMenuItem.Click += new System.EventHandler(this.sPECIFICATIIDETESTATToolStripMenuItem_Click);
            // 
            // uSERSToolStripMenuItem
            // 
            this.uSERSToolStripMenuItem.Name = "uSERSToolStripMenuItem";
            this.uSERSToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.uSERSToolStripMenuItem.Text = "USERS*";
            this.uSERSToolStripMenuItem.Click += new System.EventHandler(this.uSERSToolStripMenuItem_Click);
            // 
            // editareToolStripMenuItem
            // 
            this.editareToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cLIENTIToolStripMenuItem1,
            this.cONTRACTEToolStripMenuItem1,
            this.fACTURIToolStripMenuItem1,
            this.sTORNAREFACTURAToolStripMenuItem,
            this.aCHIZITIIToolStripMenuItem1,
            this.sPECIFICATIIDETESTATToolStripMenuItem1,
            this.uSERSToolStripMenuItem1});
            this.editareToolStripMenuItem.Name = "editareToolStripMenuItem";
            this.editareToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.editareToolStripMenuItem.Text = "Editare";
            // 
            // cLIENTIToolStripMenuItem1
            // 
            this.cLIENTIToolStripMenuItem1.Name = "cLIENTIToolStripMenuItem1";
            this.cLIENTIToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.cLIENTIToolStripMenuItem1.Text = "CLIENTI";
            this.cLIENTIToolStripMenuItem1.Click += new System.EventHandler(this.cLIENTIToolStripMenuItem1_Click);
            // 
            // cONTRACTEToolStripMenuItem1
            // 
            this.cONTRACTEToolStripMenuItem1.Name = "cONTRACTEToolStripMenuItem1";
            this.cONTRACTEToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.cONTRACTEToolStripMenuItem1.Text = "CONTRACTE";
            this.cONTRACTEToolStripMenuItem1.Click += new System.EventHandler(this.cONTRACTEToolStripMenuItem1_Click);
            // 
            // fACTURIToolStripMenuItem1
            // 
            this.fACTURIToolStripMenuItem1.Name = "fACTURIToolStripMenuItem1";
            this.fACTURIToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.fACTURIToolStripMenuItem1.Text = "FACTURI";
            this.fACTURIToolStripMenuItem1.Click += new System.EventHandler(this.fACTURIToolStripMenuItem1_Click);
            // 
            // sTORNAREFACTURAToolStripMenuItem
            // 
            this.sTORNAREFACTURAToolStripMenuItem.Name = "sTORNAREFACTURAToolStripMenuItem";
            this.sTORNAREFACTURAToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.sTORNAREFACTURAToolStripMenuItem.Text = "STORNARE FACTURA";
            this.sTORNAREFACTURAToolStripMenuItem.Click += new System.EventHandler(this.sTORNAREFACTURAToolStripMenuItem_Click);
            // 
            // aCHIZITIIToolStripMenuItem1
            // 
            this.aCHIZITIIToolStripMenuItem1.Name = "aCHIZITIIToolStripMenuItem1";
            this.aCHIZITIIToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.aCHIZITIIToolStripMenuItem1.Text = "ACHIZITII";
            this.aCHIZITIIToolStripMenuItem1.Click += new System.EventHandler(this.aCHIZITIIToolStripMenuItem1_Click);
            // 
            // sPECIFICATIIDETESTATToolStripMenuItem1
            // 
            this.sPECIFICATIIDETESTATToolStripMenuItem1.Name = "sPECIFICATIIDETESTATToolStripMenuItem1";
            this.sPECIFICATIIDETESTATToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.sPECIFICATIIDETESTATToolStripMenuItem1.Text = "SPECIFICATII DE TESTAT";
            this.sPECIFICATIIDETESTATToolStripMenuItem1.Click += new System.EventHandler(this.sPECIFICATIIDETESTATToolStripMenuItem1_Click);
            // 
            // uSERSToolStripMenuItem1
            // 
            this.uSERSToolStripMenuItem1.Name = "uSERSToolStripMenuItem1";
            this.uSERSToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.uSERSToolStripMenuItem1.Text = "USERS*";
            this.uSERSToolStripMenuItem1.Click += new System.EventHandler(this.uSERSToolStripMenuItem1_Click);
            // 
            // rapoarteListariToolStripMenuItem
            // 
            this.rapoarteListariToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fACTURIToolStripMenuItem2,
            this.bORDEROUDEFACTURIToolStripMenuItem,
            this.fACTURIINCASATEToolStripMenuItem,
            this.fACTURINEPLATITEToolStripMenuItem,
            this.fACTURISTORNATEToolStripMenuItem,
            this.rAPORTDEACHIZITIIToolStripMenuItem,
            this.rAPORTDETESTAREToolStripMenuItem});
            this.rapoarteListariToolStripMenuItem.Name = "rapoarteListariToolStripMenuItem";
            this.rapoarteListariToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.rapoarteListariToolStripMenuItem.Text = "Rapoarte/Listari";
            // 
            // fACTURIToolStripMenuItem2
            // 
            this.fACTURIToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lEIToolStripMenuItem,
            this.eUROToolStripMenuItem});
            this.fACTURIToolStripMenuItem2.Name = "fACTURIToolStripMenuItem2";
            this.fACTURIToolStripMenuItem2.Size = new System.Drawing.Size(200, 22);
            this.fACTURIToolStripMenuItem2.Text = "FACTURI";
            // 
            // lEIToolStripMenuItem
            // 
            this.lEIToolStripMenuItem.Name = "lEIToolStripMenuItem";
            this.lEIToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.lEIToolStripMenuItem.Text = "LEI";
            this.lEIToolStripMenuItem.Click += new System.EventHandler(this.lEIToolStripMenuItem_Click);
            // 
            // eUROToolStripMenuItem
            // 
            this.eUROToolStripMenuItem.Name = "eUROToolStripMenuItem";
            this.eUROToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.eUROToolStripMenuItem.Text = "EURO";
            this.eUROToolStripMenuItem.Click += new System.EventHandler(this.eUROToolStripMenuItem_Click);
            // 
            // bORDEROUDEFACTURIToolStripMenuItem
            // 
            this.bORDEROUDEFACTURIToolStripMenuItem.Name = "bORDEROUDEFACTURIToolStripMenuItem";
            this.bORDEROUDEFACTURIToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.bORDEROUDEFACTURIToolStripMenuItem.Text = "BORDEROU DE FACTURI";
            this.bORDEROUDEFACTURIToolStripMenuItem.Click += new System.EventHandler(this.bORDEROUDEFACTURIToolStripMenuItem_Click);
            // 
            // fACTURIINCASATEToolStripMenuItem
            // 
            this.fACTURIINCASATEToolStripMenuItem.Name = "fACTURIINCASATEToolStripMenuItem";
            this.fACTURIINCASATEToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.fACTURIINCASATEToolStripMenuItem.Text = "FACTURI INCASATE";
            this.fACTURIINCASATEToolStripMenuItem.Click += new System.EventHandler(this.fACTURIINCASATEToolStripMenuItem_Click);
            // 
            // fACTURINEPLATITEToolStripMenuItem
            // 
            this.fACTURINEPLATITEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cUINTARZIERIToolStripMenuItem,
            this.fARAINTARZIERIToolStripMenuItem});
            this.fACTURINEPLATITEToolStripMenuItem.Name = "fACTURINEPLATITEToolStripMenuItem";
            this.fACTURINEPLATITEToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.fACTURINEPLATITEToolStripMenuItem.Text = "FACTURI NEINCASATE";
            // 
            // cUINTARZIERIToolStripMenuItem
            // 
            this.cUINTARZIERIToolStripMenuItem.Name = "cUINTARZIERIToolStripMenuItem";
            this.cUINTARZIERIToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cUINTARZIERIToolStripMenuItem.Text = "CU INTARZIERI";
            this.cUINTARZIERIToolStripMenuItem.Click += new System.EventHandler(this.cUINTARZIERIToolStripMenuItem_Click);
            // 
            // fARAINTARZIERIToolStripMenuItem
            // 
            this.fARAINTARZIERIToolStripMenuItem.Name = "fARAINTARZIERIToolStripMenuItem";
            this.fARAINTARZIERIToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fARAINTARZIERIToolStripMenuItem.Text = "FARA INTARZIERI";
            this.fARAINTARZIERIToolStripMenuItem.Click += new System.EventHandler(this.fARAINTARZIERIToolStripMenuItem_Click);
            // 
            // fACTURISTORNATEToolStripMenuItem
            // 
            this.fACTURISTORNATEToolStripMenuItem.Name = "fACTURISTORNATEToolStripMenuItem";
            this.fACTURISTORNATEToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.fACTURISTORNATEToolStripMenuItem.Text = "FACTURI STORNATE";
            this.fACTURISTORNATEToolStripMenuItem.Click += new System.EventHandler(this.fACTURISTORNATEToolStripMenuItem_Click);
            // 
            // rAPORTDEACHIZITIIToolStripMenuItem
            // 
            this.rAPORTDEACHIZITIIToolStripMenuItem.Name = "rAPORTDEACHIZITIIToolStripMenuItem";
            this.rAPORTDEACHIZITIIToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.rAPORTDEACHIZITIIToolStripMenuItem.Text = "RAPORT DE ACHIZITII";
            this.rAPORTDEACHIZITIIToolStripMenuItem.Click += new System.EventHandler(this.rAPORTDEACHIZITIIToolStripMenuItem_Click);
            // 
            // rAPORTDETESTAREToolStripMenuItem
            // 
            this.rAPORTDETESTAREToolStripMenuItem.Name = "rAPORTDETESTAREToolStripMenuItem";
            this.rAPORTDETESTAREToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.rAPORTDETESTAREToolStripMenuItem.Text = "RAPORT DE TESTARE";
            this.rAPORTDETESTAREToolStripMenuItem.Click += new System.EventHandler(this.rAPORTDETESTAREToolStripMenuItem_Click);
            // 
            // setariToolStripMenuItem
            // 
            this.setariToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.setariToolStripMenuItem.Name = "setariToolStripMenuItem";
            this.setariToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.setariToolStripMenuItem.Text = "  Setari  ";
            this.setariToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.setariToolStripMenuItem.Click += new System.EventHandler(this.setariToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(22, 220);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(353, 54);
            this.label1.TabIndex = 2;
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // reconectare
            // 
            this.reconectare.Location = new System.Drawing.Point(150, 277);
            this.reconectare.Name = "reconectare";
            this.reconectare.Size = new System.Drawing.Size(85, 32);
            this.reconectare.TabIndex = 3;
            this.reconectare.Text = "Reconectare";
            this.reconectare.UseVisualStyleBackColor = true;
            this.reconectare.Visible = false;
            this.reconectare.Click += new System.EventHandler(this.reconectare_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(299, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 60);
            this.button1.TabIndex = 4;
            this.button1.Text = "Meniu Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WindowsFormsApp1.Properties.Resources.kissclipart_database_server_computer_servers_computer_icons_9edeeee8e60e8743;
            this.pictureBox1.Location = new System.Drawing.Point(108, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(175, 177);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // MeniulAplicatiei
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(415, 320);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.reconectare);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MeniulAplicatiei";
            this.Text = "Meniu Principal";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MeniulAplicatiei_FormClosed);
            this.Load += new System.EventHandler(this.MeniulAplicatiei_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem adaugareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLIENTIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cONTRACTEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fACTURIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aCHIZITIIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sPECIFICATIIDETESTATToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cONTRACTEToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fACTURIToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aCHIZITIIToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sPECIFICATIIDETESTATToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rapoarteListariToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fACTURIToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem lEIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eUROToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bORDEROUDEFACTURIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fACTURIINCASATEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fACTURINEPLATITEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cUINTARZIERIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fARAINTARZIERIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fACTURISTORNATEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rAPORTDEACHIZITIIToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem sTORNAREFACTURAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setariToolStripMenuItem;
        private System.Windows.Forms.Button reconectare;
        private System.Windows.Forms.ToolStripMenuItem rAPORTDETESTAREToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStripMenuItem uSERSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLIENTIToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uSERSToolStripMenuItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}