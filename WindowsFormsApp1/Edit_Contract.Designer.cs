﻿namespace WindowsFormsApp1
{
    partial class Edit_Contract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.data_incheiere = new System.Windows.Forms.DateTimePicker();
            this.adaugare_contract = new System.Windows.Forms.Button();
            this.contract_ck = new System.Windows.Forms.CheckBox();
            this.avans_ck = new System.Windows.Forms.CheckBox();
            this.total = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.avans = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nr_contract = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // data_incheiere
            // 
            this.data_incheiere.Enabled = false;
            this.data_incheiere.Location = new System.Drawing.Point(180, 45);
            this.data_incheiere.Name = "data_incheiere";
            this.data_incheiere.Size = new System.Drawing.Size(183, 20);
            this.data_incheiere.TabIndex = 79;
            // 
            // adaugare_contract
            // 
            this.adaugare_contract.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adaugare_contract.Location = new System.Drawing.Point(26, 242);
            this.adaugare_contract.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.adaugare_contract.Name = "adaugare_contract";
            this.adaugare_contract.Size = new System.Drawing.Size(337, 35);
            this.adaugare_contract.TabIndex = 76;
            this.adaugare_contract.Text = "Editare Contract";
            this.adaugare_contract.UseVisualStyleBackColor = true;
            this.adaugare_contract.Click += new System.EventHandler(this.adaugare_contract_Click);
            // 
            // contract_ck
            // 
            this.contract_ck.AutoSize = true;
            this.contract_ck.Location = new System.Drawing.Point(204, 187);
            this.contract_ck.Name = "contract_ck";
            this.contract_ck.Size = new System.Drawing.Size(92, 17);
            this.contract_ck.TabIndex = 75;
            this.contract_ck.Text = "Contract Platit";
            this.contract_ck.UseVisualStyleBackColor = true;
            // 
            // avans_ck
            // 
            this.avans_ck.AccessibleDescription = "";
            this.avans_ck.AutoSize = true;
            this.avans_ck.Location = new System.Drawing.Point(63, 187);
            this.avans_ck.Name = "avans_ck";
            this.avans_ck.Size = new System.Drawing.Size(82, 17);
            this.avans_ck.TabIndex = 74;
            this.avans_ck.Text = "Avans Platit";
            this.avans_ck.UseVisualStyleBackColor = true;
            // 
            // total
            // 
            this.total.Location = new System.Drawing.Point(180, 124);
            this.total.Multiline = true;
            this.total.Name = "total";
            this.total.ReadOnly = true;
            this.total.Size = new System.Drawing.Size(183, 21);
            this.total.TabIndex = 67;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "Total";
            // 
            // avans
            // 
            this.avans.Location = new System.Drawing.Point(180, 82);
            this.avans.Name = "avans";
            this.avans.ReadOnly = true;
            this.avans.Size = new System.Drawing.Size(183, 20);
            this.avans.TabIndex = 65;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 64;
            this.label2.Text = "Avans";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "Data Incheierii";
            // 
            // nr_contract
            // 
            this.nr_contract.Location = new System.Drawing.Point(180, 19);
            this.nr_contract.Name = "nr_contract";
            this.nr_contract.ReadOnly = true;
            this.nr_contract.Size = new System.Drawing.Size(183, 20);
            this.nr_contract.TabIndex = 81;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 80;
            this.label3.Text = "Nr. Contract";
            // 
            // Edit_Contract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 333);
            this.Controls.Add(this.nr_contract);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.data_incheiere);
            this.Controls.Add(this.adaugare_contract);
            this.Controls.Add(this.contract_ck);
            this.Controls.Add(this.avans_ck);
            this.Controls.Add(this.total);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.avans);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Edit_Contract";
            this.Text = "Edit_Contract";
            this.Load += new System.EventHandler(this.Edit_Contract_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker data_incheiere;
        private System.Windows.Forms.Button adaugare_contract;
        private System.Windows.Forms.CheckBox contract_ck;
        private System.Windows.Forms.CheckBox avans_ck;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox avans;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nr_contract;
        private System.Windows.Forms.Label label3;
    }
}