﻿namespace WindowsFormsApp1
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usr = new System.Windows.Forms.Label();
            this.prl = new System.Windows.Forms.Label();
            this.user = new System.Windows.Forms.TextBox();
            this.parola = new System.Windows.Forms.TextBox();
            this.logare = new System.Windows.Forms.Button();
            this.reconectare = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.setariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // usr
            // 
            this.usr.AutoSize = true;
            this.usr.Location = new System.Drawing.Point(31, 33);
            this.usr.Name = "usr";
            this.usr.Size = new System.Drawing.Size(55, 13);
            this.usr.TabIndex = 0;
            this.usr.Text = "Username";
            // 
            // prl
            // 
            this.prl.AutoSize = true;
            this.prl.Location = new System.Drawing.Point(31, 55);
            this.prl.Name = "prl";
            this.prl.Size = new System.Drawing.Size(37, 13);
            this.prl.TabIndex = 1;
            this.prl.Text = "Parola";
            // 
            // user
            // 
            this.user.Location = new System.Drawing.Point(121, 26);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(167, 20);
            this.user.TabIndex = 2;
            this.user.Enter += new System.EventHandler(this.user_Enter);
            // 
            // parola
            // 
            this.parola.Location = new System.Drawing.Point(121, 56);
            this.parola.Name = "parola";
            this.parola.PasswordChar = '*';
            this.parola.Size = new System.Drawing.Size(167, 20);
            this.parola.TabIndex = 3;
            this.parola.Enter += new System.EventHandler(this.parola_Enter);
            // 
            // logare
            // 
            this.logare.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logare.Location = new System.Drawing.Point(121, 82);
            this.logare.Name = "logare";
            this.logare.Size = new System.Drawing.Size(167, 35);
            this.logare.TabIndex = 4;
            this.logare.TabStop = false;
            this.logare.Text = "Login";
            this.logare.UseVisualStyleBackColor = true;
            this.logare.Click += new System.EventHandler(this.logare_Click);
            // 
            // reconectare
            // 
            this.reconectare.Location = new System.Drawing.Point(150, 200);
            this.reconectare.Name = "reconectare";
            this.reconectare.Size = new System.Drawing.Size(85, 32);
            this.reconectare.TabIndex = 6;
            this.reconectare.Text = "Reconectare";
            this.reconectare.UseVisualStyleBackColor = true;
            this.reconectare.Visible = false;
            this.reconectare.Click += new System.EventHandler(this.reconectare_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(22, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(353, 65);
            this.label1.TabIndex = 5;
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setariToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(387, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // setariToolStripMenuItem
            // 
            this.setariToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.setariToolStripMenuItem.Name = "setariToolStripMenuItem";
            this.setariToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.setariToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.setariToolStripMenuItem.Text = "Setari";
            this.setariToolStripMenuItem.Click += new System.EventHandler(this.setariToolStripMenuItem_Click);
            // 
            // Login
            // 
            this.AcceptButton = this.logare;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 243);
            this.Controls.Add(this.reconectare);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.logare);
            this.Controls.Add(this.parola);
            this.Controls.Add(this.user);
            this.Controls.Add(this.prl);
            this.Controls.Add(this.usr);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Login";
            this.Text = "Login";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Login_FormClosed);
            this.Load += new System.EventHandler(this.Login_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usr;
        private System.Windows.Forms.Label prl;
        private System.Windows.Forms.TextBox user;
        private System.Windows.Forms.TextBox parola;
        private System.Windows.Forms.Button logare;
        private System.Windows.Forms.Button reconectare;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem setariToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}