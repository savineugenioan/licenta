﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace WindowsFormsApp1
{
    public partial class MeniulAplicatiei : Form
    {
        MySqlConnection con;
        string Nivel_acces;

        public MeniulAplicatiei()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(con_DB);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_complete);
        }
        public MeniulAplicatiei(string Nivel_acces) : this()
        {
            this.Nivel_acces = Nivel_acces;
        }

        private void con_DB(object sender, DoWorkEventArgs e)
        {
            con = new MySqlConnection(Conexiune.GetConnectionString());

            if (con.State == ConnectionState.Open && con.Ping())
            {
                //good
            }
            else
            {
                con.Open();
            }
        }

        private void worker_complete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                label1.Text = "Conectarea la baza de date a fost Intrerupta";
                label1.ForeColor = Color.Yellow;
                reconectare.Visible = true;
                string eroare = e.Error.Message;
            }
            else if (e.Error != null)
            {
                label1.Text = "CONEXIUNEA LA BAZA DE DATE A ESUAT";
                label1.ForeColor = Color.Red;
                reconectare.Visible = true;
                string eroare = e.Error.Message;
            }
            else
            {
                label1.Text = "CONEXIUNEA LA BAZA DE DATE A FOST EFECTUATA CU SUCCES";
                label1.ForeColor = Color.Green;
                reconectare.Visible = false;
            }

            Globalz.del_gif(this);
        }

        private void aCHIZITIIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareAchizitii(con);
            f.ShowDialog();
        }

        private void MeniulAplicatiei_Load(object sender, EventArgs e)
        {
            switch (this.Nivel_acces)
            {
                case "1":
                    uSERSToolStripMenuItem.Visible = false;
                    uSERSToolStripMenuItem1.Visible = false;
                    break;
                case "2":
                    break;
                default:
                    uSERSToolStripMenuItem.Visible = false;
                    uSERSToolStripMenuItem1.Visible = false;
                    editareToolStripMenuItem.Visible = false;
                    break;
            }

            load_Menu();
        }

        private void load_Menu(object sender)
        {

            if (!backgroundWorker1.IsBusy)
            {
                Globalz.LoadingGif(sender);
                backgroundWorker1.RunWorkerAsync();
            }

        }

        private void load_Menu()
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }

        }

        private void MeniulAplicatiei_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void sPECIFICATIIDETESTATToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareTestat(con);
            f.ShowDialog();
            load_Menu();
        }

        private void fACTURIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareFactura(con);
            f.ShowDialog();
            load_Menu();
        }

        private void cONTRACTEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareContracte(con);
            f.ShowDialog();
            load_Menu();
        }

        private void cLIENTIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareClienti(con);
            f.ShowDialog();
            load_Menu();
        }

        private void cLIENTIToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form f = new Editare_Clienti(con);
            f.ShowDialog();
            load_Menu();
        }

        private void cONTRACTEToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form f = new Editare_Contracte(con);
            f.ShowDialog();
            load_Menu();
        }

        private void fACTURIToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form f = new Editare_Facturi(con);
            f.ShowDialog();
            load_Menu();
        }

        private void sTORNAREFACTURAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new Stornare_Facturi(con);
            f.ShowDialog();
            load_Menu();
        }

        private void aCHIZITIIToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form f = new Editare_Achizitii(con);
            f.ShowDialog();
            load_Menu();
        }

        private void sPECIFICATIIDETESTATToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form f = new Editare_Testari(con);
            f.ShowDialog();
            load_Menu();
        }

        private void eUROToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_Factura_Euro(con);
            f.ShowDialog();
            load_Menu();
        }

        private void lEIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_Factura_Lei(con);
            f.ShowDialog();
            load_Menu();
        }

        private void setariToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new Setari();
            f.ShowDialog();
            load_Menu();
        }

        private void reconectare_Click(object sender, EventArgs e)
        {
            load_Menu(sender);
        }

        private void bORDEROUDEFACTURIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_B_Facturi(con);
            f.ShowDialog();
            load_Menu();
        }

        private void fACTURIINCASATEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_F_Incasate(con);
            f.ShowDialog();
            load_Menu();
        }

        private void cUINTARZIERIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_FN_Intarzieri(con);
            f.ShowDialog();
            load_Menu();
        }

        private void fARAINTARZIERIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_FN_F_Intarzieri(con);
            f.ShowDialog();
            load_Menu();
        }

        private void fACTURISTORNATEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_F_Stornate(con);
            f.ShowDialog();
            load_Menu();
        }

        private void rAPORTDEACHIZITIIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_Achizitii(con);
            f.ShowDialog();
            load_Menu();
        }

        private void rAPORTDETESTAREToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new R_Testare(con);
            f.ShowDialog();
            load_Menu();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new Login();
            f.Show();
            this.Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            load_Menu();
        }

        private void uSERSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new Adaugare_Users(con);
            f.ShowDialog();
            load_Menu();
        }

        private void uSERSToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form f = new Editare_Users(con);
            f.ShowDialog();
            load_Menu();
        }
    }
}
