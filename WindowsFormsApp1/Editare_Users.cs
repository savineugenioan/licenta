﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Editare_Users : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        public Editare_Users()
        {
            InitializeComponent();
        }
        public Editare_Users(MySqlConnection con) : this()
        {
            this.con = con;
        }

        private void Editare_Users_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            populate_grid();
            dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[2].Visible = false;

        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            Form f = new Edit_Users(con, dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            f.ShowDialog();
            populate_grid();
        }

        private void populate_grid()
        {
            cmd = new MySqlCommand("S_Users", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", "");
            DataTable table = new DataTable();
            table.Load(cmd.ExecuteReader());
            this.dataGridView1.DataSource = table;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("Sigur vreti sa stergeti utilizatorul selectat?", "DELETE", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    try
                    {
                        cmd = new MySqlCommand("Stergere_Utilizator", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Userul a fost sters cu succes", "Stergere");
                        populate_grid();
                    }
                    catch(Exception err)
                    {
                        MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Selectati Un User Din Lista", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
