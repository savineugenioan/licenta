﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Login : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        public Login()
        {
            Conexiune.getConfiguration();
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(con_DB);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_complete);
        }

        private void con_DB(object sender, DoWorkEventArgs e)
        {
            con = new MySqlConnection(Conexiune.GetConnectionString());

            if (con.State == ConnectionState.Open && con.Ping())
            {
                //goodds
            }
            else
            {
                con.Open();
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private void worker_complete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                label1.Text = "Conectarea la baza de date a fost Intrerupta";
                label1.ForeColor = Color.Yellow;
                reconectare.Visible = true;
                string eroare = e.Error.Message;
            }
            else if (e.Error != null)
            {
                label1.Text = "CONEXIUNEA LA BAZA DE DATE A ESUAT";
                label1.ForeColor = Color.Red;
                reconectare.Visible = true;
                string eroare = e.Error.Message;
            }
            else
            {
                label1.Text = "CONEXIUNEA LA BAZA DE DATE A FOST EFECTUATA CU SUCCES";
                label1.ForeColor = Color.Green;
                reconectare.Visible = false;
            }

            Globalz.del_gif(this);
        }
        private void logare_Click(object sender, EventArgs e)
        {
            try
            {
                if (con.Ping())
                {
                    MD5 hash = MD5.Create();
                    string pass = GetMd5Hash(hash, parola.Text.Trim());
                    //verific daca userul a fost introdus corect
                    cmd = new MySqlCommand("Login_Verif_Users", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("utilizator", user.Text.Trim());
                    cmd.Parameters.AddWithValue("par", pass);
                    rdr = cmd.ExecuteReader();
                    if(rdr.HasRows)
                    {
                        rdr.Read();
                        Form f = new MeniulAplicatiei(rdr[5].ToString());
                        con.Close();
                        f.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Datele introduse nu sunt corecte !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        parola.SelectAll();
                    }
                    rdr.Close();
                }
                else
                {
                    load_Menu(sender);
                }
            }
            catch(Exception err) 
            {
                string eroare = err.Message;
                load_Menu(sender);
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            load_Menu(sender);
        }

        private void load_Menu(object sender)
        {
            
            if (!backgroundWorker1.IsBusy)
            {
                Globalz.LoadingGif(sender);
                backgroundWorker1.RunWorkerAsync();
            }

        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void setariToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new Setari();
            f.ShowDialog();
            load_Menu(sender);
        }

        private void reconectare_Click(object sender, EventArgs e)
        {
            load_Menu(sender);
        }

        private void user_Enter(object sender, EventArgs e)
        {
            user.SelectAll();
        }

        private void parola_Enter(object sender, EventArgs e)
        {
            parola.SelectAll();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
