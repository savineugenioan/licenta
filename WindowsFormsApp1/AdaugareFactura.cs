﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AdaugareFactura : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;

        int client_fidel;
        List<string> dictionar_servicii;
        Dictionary<string, string> preturi_servicii;

        string tip_factura_text;
        public AdaugareFactura()
        {
            InitializeComponent();
        }
        public AdaugareFactura(MySqlConnection con) : this()
        {
            this.con = con;
        }
        private void AdaugareFactura_Load(object sender, EventArgs e)
        {
            try
            {
                search.SelectedItem = "NR. CONTRACT";

                // adaug DropDown a doua celula - Serviciul 
                data_s.Format = DateTimePickerFormat.Custom;
                data_s.CustomFormat = "yyyy-MM-dd";
                tip_factura.SelectedItem = tip_factura.Items[0];
                tip_factura.SelectedItem = tip_factura.Items[1];
                tip_factura_text = "STANDARD";
                stare_plata.SelectedIndex = 1;

                if (con.State == ConnectionState.Open)
                {
                    cmd = new MySqlCommand("S_Servicii", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    rdr = cmd.ExecuteReader();
                    //Populez dictionarul de servicii
                    dictionar_servicii = new List<string>();
                    preturi_servicii = new Dictionary<string, string>();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            dictionar_servicii.Add(rdr[0].ToString() + " " + rdr[1].ToString());
                            //populez preturile pt clienti/fideli
                            preturi_servicii.Add(rdr[0].ToString() + "_0", rdr[2].ToString());
                            preturi_servicii.Add(rdr[0].ToString() + "_1", rdr[3].ToString());
                        }
                    }
                    rdr.Close();

                    var c1 = new DataGridViewComboBoxColumn();
                    c1.Name = "Serviciu";
                    c1.HeaderText = "Serviciu";
                    c1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    c1.DataSource = dictionar_servicii;
                    date_factura.Columns.Insert(1, c1);

                    //termin de adaugat Drop Down in grid - Serviciile

                    DateTime dateTime = DateTime.UtcNow.Date;
                    serie.Text = dateTime.ToString("yyyy");
                    dateTime = data_s.Value;

                    // CALCULEZ SERIA SI NUMARUL
                    cmd = new MySqlCommand("Max_Index", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Tabela", "facturi");
                    int i = Int32.Parse(cmd.ExecuteScalar().ToString()) + 1;
                    numar.Text = i.ToString();

                }
                else
                {
                    MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //verificari de inceput

                //factura standard
                if (tip_factura.Text != "AVANS")
                {
                    if (search.SelectedItem.ToString() == "NR. CONTRACT" && Int32.TryParse(nr_contract_clienti.Text, out int res) == false && nr_contract_clienti.Text != "")
                    {
                        MessageBox.Show("Introduceti Numarul Contractului ( Corect! )", "Nr Contract", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (nr_contract_clienti.Text == "" && search.SelectedItem.ToString() != "NR. CONTRACT")
                    {
                        MessageBox.Show("Introduceti un Client ", "Client", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (date_factura.Rows.Count ==1 && Globalz.EmptyCellsNoAdd(date_factura, 4))
                    {
                        MessageBox.Show("Introduceti date in factura !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (Globalz.EmptyCells(date_factura, 4))
                    {
                        MessageBox.Show("Toate celulele trebuie sa contina date !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (search.SelectedItem.ToString() == "NR. CONTRACT" && nr_contract_clienti.Text == "")
                    {
                        MessageBox.Show("Introduceti un numar de Contract !!", "INSERT", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }



                //factura avans
                if(tip_factura.Text == "AVANS")
                {
                    if(nr_contract_clienti.Text == "")
                    {
                        MessageBox.Show("Introduceti un Nr de Contract ", "Nr. Contract", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (Int32.TryParse(nr_contract_clienti.Text, out int res) == false)
                    {
                        MessageBox.Show("Introduceti Numarul Contractului ( Corect! )", "Nr Contract", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (textBox1.Text == "")
                    {
                        MessageBox.Show("Introduceti o Valoare a Avansului ", "Avans", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }


                double valoare_achizitie = 0;
                int achitat = 0;
                if (stare_plata.SelectedItem.ToString() == "FACTURA ACHITATA")
                {
                    achitat = 1;
                }

                if (con.State == ConnectionState.Open)
                {
                    try
                    {
                        if (groupBox2.Visible == true) // pentru facturi STANDARD
                        {
                            var rows = date_factura.Rows;
                            client_fidel = 0;


                            // val achizitie
                            for (int j = 0; j < rows.Count - 1; j++)
                            {
                                string[] cells = new string[5];
                                for (int i = 0; i < 4; i++)
                                {
                                    cells[i] = rows[j].Cells[i].Value.ToString();

                                }

                                if (rows[j].Cells[4].Value != null)
                                {
                                    cells[4] = rows[j].Cells[4].Value.ToString();
                                }
                                else
                                {
                                    cells[4] = string.Empty;
                                }
                                valoare_achizitie += Int32.Parse(cells[2]) * Int32.Parse(cells[3]);
                            }


                            //insert in FACTURI

                            cmd = new MySqlCommand("I_Facturi", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@serie_numar", serie.Text + '/' + numar.Text);
                            cmd.Parameters.AddWithValue("@valoare_achizitie", valoare_achizitie);
                            cmd.Parameters.AddWithValue("@penalizari", "0");
                            cmd.Parameters.AddWithValue("@tip_factura", tip_factura.SelectedItem.ToString());
                            cmd.Parameters.AddWithValue("@achitat", achitat);
                            cmd.Parameters.AddWithValue("@data_s", data_s.Text);
                            cmd.Parameters.AddWithValue("@nr_contract_client", nr_contract_clienti.Text);
                            cmd.Parameters.AddWithValue("@tip_insert", search.SelectedItem.ToString());
                            cmd.ExecuteNonQuery();



                            for (int j = 0; j < rows.Count - 1; j++)
                            {
                                string[] cells = new string[5];
                                for (int i = 0; i < 4; i++)
                                {
                                    cells[i] = rows[j].Cells[i].Value.ToString();

                                }

                                if (rows[j].Cells[4].Value != null)
                                {
                                    cells[4] = rows[j].Cells[4].Value.ToString();
                                }
                                else
                                {
                                    cells[4] = string.Empty;
                                }

                                cmd = new MySqlCommand("I_Facturi_Detalii", con);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@Nr_Crt", cells[0].Substring(0, 1));
                                cmd.Parameters.AddWithValue("@Denumire", cells[1].Substring(cells[1].IndexOf(' ') + 1));
                                cmd.Parameters.AddWithValue("@Cantitate_Ore", cells[2]);
                                cmd.Parameters.AddWithValue("@Pretul_Unitar", cells[3]);
                                cmd.Parameters.AddWithValue("@Comentarii", cells[4].ToString());
                                cmd.Parameters.AddWithValue("@Serie_Numar", serie.Text + '/' + numar.Text);
                                cmd.Parameters.AddWithValue("@Id_Serviciu", cells[1].Substring(0, cells[1].IndexOf(' ')));
                                cmd.ExecuteNonQuery();
                            }

                        }
                        else // pentru facturi AVANS
                        {
                            //valoare achizitie
                            valoare_achizitie = Int32.Parse(textBox1.Text);

                            //INSERT in FACTURI

                            cmd = new MySqlCommand("I_Facturi", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@serie_numar", serie.Text + '/' + numar.Text);
                            cmd.Parameters.AddWithValue("@valoare_achizitie", valoare_achizitie);
                            cmd.Parameters.AddWithValue("@penalizari", "0");
                            cmd.Parameters.AddWithValue("@tip_factura", tip_factura.SelectedItem.ToString());
                            cmd.Parameters.AddWithValue("@achitat", achitat);
                            cmd.Parameters.AddWithValue("@data_s", data_s.Text);
                            cmd.Parameters.AddWithValue("@nr_contract_client", nr_contract_clienti.Text);
                            cmd.Parameters.AddWithValue("@tip_insert", search.SelectedItem.ToString());
                            cmd.ExecuteNonQuery();


                            //INSERT IN FACTURI_DETALII
                            cmd = new MySqlCommand("I_Facturi_Detalii", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Nr_Crt", "1");
                            cmd.Parameters.AddWithValue("@Denumire", "AVANS");
                            cmd.Parameters.AddWithValue("@Cantitate_Ore", "1");
                            cmd.Parameters.AddWithValue("@Pretul_Unitar", valoare_achizitie);
                            cmd.Parameters.AddWithValue("@Comentarii", "AVANS");
                            cmd.Parameters.AddWithValue("@Serie_Numar", serie.Text + '/' + numar.Text);
                            cmd.Parameters.AddWithValue("@Id_Serviciu",null);
                            cmd.ExecuteNonQuery();
                        }

                        MessageBox.Show("Datele au fost introduse cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                        this.Close();
                    }
                    catch (Exception err)
                    {
                        if(err.Message.Contains("Check constraint 'facturi_chk_1' is violated"))
                        {
                            MessageBox.Show("Totalul facturii este 0. Introduceti date in factura !", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        else if(err.Message.Contains("Cannot add or update a child row: a foreign key constraint fails"))
                        {
                            MessageBox.Show($"{search.Text} nu exista in baza de date ! ","EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        else
                        {
                            MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }


                    }
                }
                else
                {
                    MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //SCHIMB INTERFATA in FUNCTIE DE TIPUL FACTURII
        private void tip_factura_SelectedIndexChanged(object sender, EventArgs e)

        {
            string x = tip_factura.SelectedItem.ToString();
            if (x != this.tip_factura_text && x == "AVANS")
            {
                groupBox2.Visible = false;
                groupBox1.Visible = true;
                search.SelectedItem = "NR. CONTRACT";
                search.Enabled = false;
                button1.Top += (groupBox1.Size.Height - groupBox2.Size.Height);
                this.tip_factura_text = x;
                cauta_client.Visible = false;
            }
            else if (x != this.tip_factura_text && x == "STANDARD")
            {
                groupBox2.Visible = true;
                groupBox1.Visible = false;
                search.SelectedItem = "NR. CONTRACT";
                search.Enabled = true;
                button1.Top += (groupBox2.Size.Height - groupBox1.Size.Height);
                this.tip_factura_text = x;
                cauta_client.Visible = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareClienti(con);
            f.ShowDialog();
        }

        // EVENT verific daca e numar
        private void Not_Number_Press(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        // EVENT declansat cand scriu in CELL in GRID
        private void date_factura_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) // 
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Not_Number_Press);
            if (date_factura.CurrentCell.ColumnIndex == 0 || date_factura.CurrentCell.ColumnIndex == 2 || date_factura.CurrentCell.ColumnIndex == 3)
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Not_Number_Press);
                }
            }
        }

        private void nr_contract_TextChanged(object sender, EventArgs e)
        {
            date_factura.Rows.Clear();

                if (Int32.TryParse(nr_contract_clienti.Text, out int res) == true && groupBox2.Visible == false && tip_factura_text!="AVANS")
                    groupBox2.Visible = true;
        }

        private void nr_contract_KeyPress(object sender, KeyPressEventArgs e)
        {
            date_factura.Visible = false;
            if (search.SelectedItem.ToString() == "NR. CONTRACT")
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
        }

/*        private void nr_contract_KeyUp(object sender, KeyEventArgs e)
        {
            verifClientFidel();
        }*/

        private void date_factura_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            //calcule pretul afisat in grid
            if (date_factura.CurrentCell.ColumnIndex == 1)
                if (date_factura.CurrentCell.EditedFormattedValue != null)
                {
                    string y = date_factura.CurrentCell.EditedFormattedValue.ToString();
                    date_factura[3, date_factura.CurrentCell.RowIndex].Value =
                        preturi_servicii[y.Substring(0, y.IndexOf(' ')) + "_" + client_fidel.ToString()];
                    date_factura.CurrentCell = date_factura[2, date_factura.CurrentCell.RowIndex];
                }
        }

        private void tip_factura_TextChanged(object sender, EventArgs e)
        {
            if(tip_factura.Text == "AVANS")
            {
                date_factura.Rows.Clear();
            }
        }

        private void search_SelectedIndexChanged(object sender, EventArgs e)
        {
            nr_contract_clienti.Text = "";
            date_factura.Visible = false;
            if (search.SelectedItem.ToString() != "NR. CONTRACT")
            {
                nr_contract_clienti.ReadOnly = true;
                cauta_client.Text = "Cauta Client";

            }
            else
            {
                nr_contract_clienti.ReadOnly = false;
                cauta_client.Text = "Verifica Contract";
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void cauta_client_Click(object sender, EventArgs e)
        {
            
            if (search.SelectedItem.ToString() != "NR. CONTRACT")
            {
                nr_contract_clienti.Text = "";
                using (var form = new Get_Id_Client(con))
                {
                    form.ShowDialog();
                    string val = form.ReturnValue1;
                    this.nr_contract_clienti.Text = val;
                    if(val != "")
                    {
                        date_factura.Visible = true;
                    }
                    else
                    {
                        date_factura.Visible = false;
                    }
                }
            }
            else
            {
                cmd = new MySqlCommand("Verif_Contract", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nr_Contr", nr_contract_clienti.Text);
                rdr = cmd.ExecuteReader();
                if(rdr.HasRows)
                {
                    date_factura.Visible = true;
                }
                else
                {
                    date_factura.Visible = false;
                    MessageBox.Show("Contractul nu exista in baza de date", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                rdr.Close();
            }
            verifClientFidel();

        }

        private void verifClientFidel()
        {
            if (nr_contract_clienti.Text == "")
            {
                client_fidel = 0;
                return;
            }

            //vad daca este client_fidel

            cmd = new MySqlCommand("Verfi_C_Fidel", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nr_contract_clienti", nr_contract_clienti.Text);
            cmd.Parameters.AddWithValue("@tip_insert", search.SelectedItem.ToString());
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                rdr.Read();

                if (rdr[0].ToString() == "1")
                {
                    client_fidel = 1;
                }
                else
                {
                    client_fidel = 0;
                }
            }
            else
            {
                client_fidel = 0;
            }
            rdr.Close();

        }

        private void date_factura_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            date_factura.CurrentRow.Cells[0].Value = date_factura.CurrentRow.Index +1;
        }

        private void date_factura_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            foreach(DataGridViewRow row in date_factura.Rows)
            {
                row.Cells[0].Value = row.Index + 1;
            }
            date_factura.Rows[date_factura.Rows.Count-1].Cells[0].Value = "";

        }
    }
}
