﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Office.Interop.Excel;
using MySql.Data.MySqlClient;
using DataTable = System.Data.DataTable;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp1
{
    public partial class R_Factura_Lei : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        double x;
        DataGridViewRow selectedRow;
        public R_Factura_Lei()
        {
            InitializeComponent();
        }
        public R_Factura_Lei(MySqlConnection con) : this()
        {
            this.con = con;
        }

        private void R_Factura_Lei_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            search.SelectedItem = "DENUMIRE CLIENT";
            d_scadenta.Format = DateTimePickerFormat.Custom;
            d_scadenta.CustomFormat = "yyyy-MM-dd";
            d_scadenta.Value = DateTime.Today.AddMonths(-3);

            try
            {
                populate_grid();
                dataGridView1.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void populate_grid()
        {
            string search_item;
            x = Globalz.getEuro();
            if (search.SelectedItem.ToString() == "DENUMIRE CLIENT")
            {
                search_item = "cl.Denumire";
            }
            else
            {
                search_item = "f.Nr_Contract";
            }

            cmd = new MySqlCommand("S_Facturi", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@search_item", search_item);
            cmd.Parameters.AddWithValue("@d_inceput", d_scadenta.Text);
            cmd.Parameters.AddWithValue("@d_final", d_scadenta.MaxDate.GetDateTimeFormats('u')[0]);
            cmd.Parameters.AddWithValue("@s_param", client.Text.Trim());
            DataTable table = new DataTable();
            table.Load(cmd.ExecuteReader());
            this.dataGridView1.DataSource = table;

            dataGridView1.Columns[1].HeaderText = "Total Lei";
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                double total = double.Parse(row.Cells[1].Value.ToString()) * x;
                double penalizari = double.Parse(row.Cells[2].Value.ToString()) * x;
                row.Cells[1].Value = Math.Round(total);
                row.Cells[2].Value = Math.Round(penalizari);
            }
        }

        private void search_SelectedIndexChanged(object sender, EventArgs e)
        {
            client.Text = "";
        }

        private void client_TextChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void d_scadenta_ValueChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            string path;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }

            try
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                selectedRow = dataGridView1.Rows[selectedrowindex];
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


                xlWorkSheet.get_Range("A1", "A1").Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWorkSheet.get_Range("A1", "A1").Style.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlWorkSheet.get_Range("a1", "e2").Merge(false);
                xlWorkSheet.Cells[1, 1] = "Factura " + selectedRow.Cells[0].Value.ToString();
                xlWorkSheet.Cells[1, 1].Font.Size = 20;
                xlWorkSheet.Cells[1, 1].Font.Bold = true;
                xlWorkSheet.Cells[4, 1] = "Nr.Crt.";
                xlWorkSheet.Cells[4, 2] = "Denumire";
                xlWorkSheet.Cells[4, 3] = "Cantitate/Ore";
                xlWorkSheet.Cells[4, 4] = "Pret Unitar(Lei)";
                xlWorkSheet.Cells[4, 5] = "Comentarii";
                var chartRange = xlWorkSheet.get_Range("a4", "e4");
                chartRange.Columns.AutoFit();
                xlWorkSheet.Columns[2].ColumnWidth = 18;//denumire
                xlWorkSheet.Columns[5].ColumnWidth = 35;//comentarii

                //get Data
                cmd = new MySqlCommand("S_Facturi_D_Prod", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", selectedRow.Cells[0].Value.ToString());
                rdr = cmd.ExecuteReader();
                int i = 5;
                int str = i;
                while (rdr.Read())
                {
                    //populare tabel
                    xlWorkSheet.Cells[i, 1] = rdr[1];
                    xlWorkSheet.Cells[i, 2] = rdr[2];
                    xlWorkSheet.Cells[i, 3] = rdr[3];
                    xlWorkSheet.Cells[i, 4] = Math.Round(double.Parse(rdr[4].ToString())*x);
                    xlWorkSheet.Cells[i, 5] = rdr[5];
                    i++;
                }
                // bag penalizarile
                xlWorkSheet.Cells[i, 1] = int.Parse(rdr[1].ToString()) + 1;
                xlWorkSheet.Cells[i, 2] = "Penalizari";
                xlWorkSheet.Cells[i, 3] = "1";
                xlWorkSheet.Cells[i, 4] = Math.Round(double.Parse(selectedRow.Cells[2].Value.ToString()));
                xlWorkSheet.Cells[i, 5] = "Penalizari";
                i++;
                rdr.Close();


                //bag total
                xlWorkSheet.get_Range($"a{i}", $"e{i}").Merge(false);
                xlWorkSheet.get_Range($"b{i + 1}", $"e{i + 1}").Merge(false);
                xlWorkSheet.get_Range($"a{i + 1}", $"e{i + 1}").Font.Bold = true;
                xlWorkSheet.Cells[i+1, 1] = "TOTAL";
                xlWorkSheet.Cells[i+1, 2] = double.Parse(selectedRow.Cells[1].Value.ToString()) +
                    Math.Round(double.Parse(selectedRow.Cells[2].Value.ToString())) + " Lei";

                //formatari tabel
                Range rng = xlWorkSheet.get_Range("A4", $"E{i + 1}");
                rng.WrapText = true;
                rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //pt print repeat
                xlWorkSheet.PageSetup.PrintTitleRows = "$1:$4";

                //salvare fisier
                xlWorkBook.SaveAs($"{path}\\Factura_{selectedRow.Cells[0].Value.ToString().Replace('/', '.')}_in_Lei.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                MessageBox.Show($"Fisier XL creat : {path}\\Factura_{selectedRow.Cells[0].Value.ToString().Replace('/', '.')}_in_Lei.xls");
            }
            catch (Exception ex)
            {
                if(ex.Message.Contains("Cannot access"))
                {
                    MessageBox.Show($"Fisierul XL nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                        $"Factura_{selectedRow.Cells[0].Value.ToString().Replace('/', '.')}_in_Lei.xls si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if(ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path;
            int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
            selectedRow = dataGridView1.Rows[selectedrowindex];
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }
            try
            {
                if (File.Exists($"{path}\\Factura_{selectedRow.Cells[0].Value.ToString().Replace('/', '.')}_in_Lei.pdf"))
                {
                    DialogResult result = MessageBox.Show("Fisierul deja exista. Doriti sa il rescrieti?", "Confirmare", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (result == DialogResult.No)
                    {
                        return;
                    }
                }
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);
                FileStream fs = new System.IO.FileStream($"{path}\\Factura_{selectedRow.Cells[0].Value.ToString().Replace('/', '.')}_in_Lei.pdf", System.IO.FileMode.Create);

                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                document.SetPageSize(PageSize.A4.Rotate());

                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                // Open the document to enable you to write to the document  
                document.Open();
                // Add a simple and wellknown phrase to the document in a flow layout manner  
                Paragraph y = new Paragraph("Factura " + selectedRow.Cells[0].Value.ToString(), boldFont);
                y.Alignment = Element.ALIGN_CENTER;
                document.Add(y);
                document.Add(new Paragraph(" "));

                PdfPTable table1 = new PdfPTable(5);
                table1.HeaderRows = 1;


                table1.AddCell("Nr.Crt.");
                table1.AddCell("Denumire");
                table1.AddCell("Cantitate/Ore");
                table1.AddCell("Pret Unitar(Euro)");
                table1.AddCell("Comentarii");


                cmd = new MySqlCommand("S_Facturi_D_Prod", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", selectedRow.Cells[0].Value.ToString());
                rdr = cmd.ExecuteReader();
                int i = 5;
                int str = i;
                while (rdr.Read())
                {
                    //populare tabel
                    table1.AddCell(rdr[1].ToString());
                    table1.AddCell(rdr[2].ToString());
                    table1.AddCell(rdr[3].ToString());
                    table1.AddCell(Math.Round(double.Parse(rdr[4].ToString())*x).ToString());
                    table1.AddCell(rdr[5].ToString());
                }
                // bag penalizarile
                table1.AddCell((int.Parse(rdr[1].ToString()) + 1).ToString());
                table1.AddCell("Penalizari");
                table1.AddCell("1");
                table1.AddCell((Math.Round(double.Parse(selectedRow.Cells[2].Value.ToString()))).ToString());
                table1.AddCell("Penalizari");
                rdr.Close();

                //bag total
                table1.AddCell("TOTAL");
                PdfPCell cell = new PdfPCell(new Phrase((double.Parse(selectedRow.Cells[1].Value.ToString()) +
                         Math.Round(double.Parse(selectedRow.Cells[2].Value.ToString())) + " Lei").ToString()))
                { Colspan = 4 };
                cell.HorizontalAlignment = 1;
                table1.AddCell(cell);

                document.Add(table1);
                document.Close();
                writer.Close();
                fs.Close();
                MessageBox.Show($"Fisier PDF creat : {path}\\Factura_{selectedRow.Cells[0].Value.ToString().Replace('/', '.')}_in_Lei.pdf");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("cannot access"))
                {
                    MessageBox.Show($"Fisierul XL nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                       $"Factura_{selectedRow.Cells[0].Value.ToString().Replace('/', '.')}_in_Lei.pdf si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
