﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Edit_Testari : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        string id;
        public Edit_Testari()
        {
            InitializeComponent();
        }

        public Edit_Testari(MySqlConnection _con, DataGridViewRow row) : this()
        {
            this.con = _con;
            id = row.Cells[0].Value.ToString();
        }
        private void Edit_Testari_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            try
            {
                cmd = new MySqlCommand("S_Testari_Detalii", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    testari.Rows.Add(rdr[0].ToString(), rdr[1].ToString(), rdr[2].ToString(), rdr[3], rdr[4].ToString());
                }
                rdr.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Globalz.EmptyCellsNoAdd(testari,3))
            {
                MessageBox.Show("Toate celulele trebuie sa contina date !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                foreach (DataGridViewRow row in testari.Rows)
                {
                    cmd = new MySqlCommand("U_Testari_Detalii", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Procedura", row.Cells[1].Value.ToString());
                    cmd.Parameters.AddWithValue("@Rezultatul_Asteptat", row.Cells[2].EditedFormattedValue.ToString());
                    cmd.Parameters.AddWithValue("@Pass", row.Cells[3].Value.ToString());
                    cmd.Parameters.AddWithValue("@Rezultate_Efective", row.Cells[4].EditedFormattedValue.ToString());
                    cmd.Parameters.AddWithValue("@id", row.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                }

                MessageBox.Show("Specificatia de Testare a fost editata cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
