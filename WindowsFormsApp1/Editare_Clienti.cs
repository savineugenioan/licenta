﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Editare_Clienti : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        public Editare_Clienti()
        {
            InitializeComponent();
        }
        public Editare_Clienti(MySqlConnection _con) :this()
        {
            this.con = _con;
        }

        private void Edit_Clienti_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            try
            {
                populate_grid();
                dataGridView1.Columns[2].Width = 300;
                dataGridView1.Columns[4].Width = 80;
                dataGridView1.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            populate_grid();
        }
        private void populate_grid()
        {
            cmd = new MySqlCommand("S_Clienti", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@s_param", CUI.Text.Trim());
            DataTable table = new DataTable();
            table.Load(cmd.ExecuteReader());
            this.dataGridView1.DataSource = table;
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow row = dataGridView1.SelectedRows[0];
            Form f = new Edit_Client(con, row);
            f.ShowDialog();
            CUI.Text = "";
            populate_grid();
        }
    }
}
