﻿namespace WindowsFormsApp1
{
    partial class AdaugareFactura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.numar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.serie = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.date_factura = new System.Windows.Forms.DataGridView();
            this.Nr_Crt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pret = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comentarii = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.data_s = new System.Windows.Forms.DateTimePicker();
            this.tip_factura = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.val_avans = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.stare_plata = new System.Windows.Forms.ComboBox();
            this.nr_contract_clienti = new System.Windows.Forms.TextBox();
            this.search = new System.Windows.Forms.ComboBox();
            this.cauta_client = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.date_factura)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Data Scadenta";
            // 
            // numar
            // 
            this.numar.Location = new System.Drawing.Point(242, 87);
            this.numar.Name = "numar";
            this.numar.ReadOnly = true;
            this.numar.Size = new System.Drawing.Size(200, 20);
            this.numar.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Numar";
            // 
            // serie
            // 
            this.serie.Location = new System.Drawing.Point(242, 50);
            this.serie.Name = "serie";
            this.serie.ReadOnly = true;
            this.serie.Size = new System.Drawing.Size(200, 20);
            this.serie.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Serie";
            // 
            // date_factura
            // 
            this.date_factura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.date_factura.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nr_Crt,
            this.Ore,
            this.Pret,
            this.Comentarii});
            this.date_factura.Location = new System.Drawing.Point(2, 9);
            this.date_factura.Name = "date_factura";
            this.date_factura.Size = new System.Drawing.Size(540, 131);
            this.date_factura.TabIndex = 48;
            this.date_factura.Visible = false;
            this.date_factura.CurrentCellDirtyStateChanged += new System.EventHandler(this.date_factura_CurrentCellDirtyStateChanged);
            this.date_factura.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.date_factura_EditingControlShowing);
            this.date_factura.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.date_factura_UserAddedRow);
            this.date_factura.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.date_factura_UserDeletedRow);
            // 
            // Nr_Crt
            // 
            this.Nr_Crt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Nr_Crt.Frozen = true;
            this.Nr_Crt.HeaderText = "Nr. Crt.";
            this.Nr_Crt.Name = "Nr_Crt";
            this.Nr_Crt.ReadOnly = true;
            this.Nr_Crt.Width = 65;
            // 
            // Ore
            // 
            this.Ore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Ore.HeaderText = "Ore";
            this.Ore.Name = "Ore";
            this.Ore.Width = 49;
            // 
            // Pret
            // 
            this.Pret.HeaderText = "Pretul pe Ora";
            this.Pret.Name = "Pret";
            // 
            // Comentarii
            // 
            this.Comentarii.HeaderText = "Comentarii*";
            this.Comentarii.MinimumWidth = 8;
            this.Comentarii.Name = "Comentarii";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(20, 394);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(544, 35);
            this.button1.TabIndex = 51;
            this.button1.Text = "Adaugare Factura";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // data_s
            // 
            this.data_s.Location = new System.Drawing.Point(242, 118);
            this.data_s.Name = "data_s";
            this.data_s.Size = new System.Drawing.Size(200, 20);
            this.data_s.TabIndex = 52;
            // 
            // tip_factura
            // 
            this.tip_factura.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tip_factura.FormattingEnabled = true;
            this.tip_factura.Items.AddRange(new object[] {
            "AVANS",
            "STANDARD"});
            this.tip_factura.Location = new System.Drawing.Point(242, 12);
            this.tip_factura.Name = "tip_factura";
            this.tip_factura.Size = new System.Drawing.Size(200, 21);
            this.tip_factura.TabIndex = 53;
            this.tip_factura.SelectedIndexChanged += new System.EventHandler(this.tip_factura_SelectedIndexChanged);
            this.tip_factura.TextChanged += new System.EventHandler(this.tip_factura_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(67, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 54;
            this.label4.Text = "Tipul de Factura";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.val_avans);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(61, 218);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 34);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            // 
            // val_avans
            // 
            this.val_avans.AutoSize = true;
            this.val_avans.Location = new System.Drawing.Point(6, 14);
            this.val_avans.Name = "val_avans";
            this.val_avans.Size = new System.Drawing.Size(76, 13);
            this.val_avans.TabIndex = 1;
            this.val_avans.Text = "Valoare Avans";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(181, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.date_factura);
            this.groupBox2.Location = new System.Drawing.Point(14, 223);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(548, 145);
            this.groupBox2.TabIndex = 56;
            this.groupBox2.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(67, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 58;
            this.label5.Text = "Stare Plata";
            // 
            // stare_plata
            // 
            this.stare_plata.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stare_plata.FormattingEnabled = true;
            this.stare_plata.Items.AddRange(new object[] {
            "FACTURA ACHITATA",
            "FACTURA NEACHITATA"});
            this.stare_plata.Location = new System.Drawing.Point(242, 152);
            this.stare_plata.Name = "stare_plata";
            this.stare_plata.Size = new System.Drawing.Size(200, 21);
            this.stare_plata.TabIndex = 59;
            // 
            // nr_contract_clienti
            // 
            this.nr_contract_clienti.Location = new System.Drawing.Point(242, 181);
            this.nr_contract_clienti.Name = "nr_contract_clienti";
            this.nr_contract_clienti.Size = new System.Drawing.Size(200, 20);
            this.nr_contract_clienti.TabIndex = 61;
            this.nr_contract_clienti.TextChanged += new System.EventHandler(this.nr_contract_TextChanged);
            this.nr_contract_clienti.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nr_contract_KeyPress);
            // 
            // search
            // 
            this.search.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.search.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.Items.AddRange(new object[] {
            "ID CLIENT",
            "NR. CONTRACT"});
            this.search.Location = new System.Drawing.Point(70, 181);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(128, 21);
            this.search.TabIndex = 62;
            this.search.SelectedIndexChanged += new System.EventHandler(this.search_SelectedIndexChanged);
            // 
            // cauta_client
            // 
            this.cauta_client.Location = new System.Drawing.Point(465, 181);
            this.cauta_client.Name = "cauta_client";
            this.cauta_client.Size = new System.Drawing.Size(97, 23);
            this.cauta_client.TabIndex = 63;
            this.cauta_client.Text = "Verifica Contract";
            this.cauta_client.UseVisualStyleBackColor = true;
            this.cauta_client.Click += new System.EventHandler(this.cauta_client_Click);
            // 
            // AdaugareFactura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(580, 447);
            this.Controls.Add(this.cauta_client);
            this.Controls.Add(this.search);
            this.Controls.Add(this.nr_contract_clienti);
            this.Controls.Add(this.stare_plata);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tip_factura);
            this.Controls.Add(this.data_s);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.serie);
            this.Controls.Add(this.label1);
            this.MinimizeBox = false;
            this.Name = "AdaugareFactura";
            this.Text = "Adaugare Factura";
            this.Load += new System.EventHandler(this.AdaugareFactura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.date_factura)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox numar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox serie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView date_factura;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker data_s;
        private System.Windows.Forms.ComboBox tip_factura;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label val_avans;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox stare_plata;
        private System.Windows.Forms.TextBox nr_contract_clienti;
        private System.Windows.Forms.ComboBox search;
        private System.Windows.Forms.Button cauta_client;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nr_Crt;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pret;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comentarii;
    }
}