﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Setari : Form
    {
        Configuration config;
        public Setari()
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Properties.Settings.Default.Server = textBox1.Text;
            Properties.Settings.Default.Port = textBox2.Text;
            Properties.Settings.Default.DB = textBox3.Text;
            Properties.Settings.Default.User = textBox4.Text;
            Properties.Settings.Default.Parola = textBox5.Text;
            Properties.Settings.Default.Save();
            string cs = $"Server={textBox1.Text};Port={textBox2.Text};Database={textBox3.Text};" +
                $"Uid={textBox4.Text};Pwd={textBox5.Text};Connection Timeout=30";
            Conexiune.SaveConnectionString(cs);
            this.Close();
        }


        private void Setari_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Settings.Default.Server;
            textBox2.Text = Properties.Settings.Default.Port;
            textBox3.Text = Properties.Settings.Default.DB;
            textBox4.Text = Properties.Settings.Default.User;
            textBox5.Text = Properties.Settings.Default.Parola;
        }
    }
}
