﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Editare_Facturi : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        public Editare_Facturi()
        {
            InitializeComponent();
        }
        public Editare_Facturi(MySqlConnection _con) : this()
        {
            this.con = _con;
        }

        private void Editare_Facturi_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            search.SelectedItem = "DENUMIRE CLIENT";
            d_scadenta.Format = DateTimePickerFormat.Custom;
            d_scadenta.CustomFormat = "yyyy-MM-dd";
            d_scadenta.Value = DateTime.Today.AddMonths(-3);

            try
            {
                populate_grid();
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void populate_grid()
        {
            string search_item;
            if (search.SelectedItem.ToString() == "DENUMIRE CLIENT")
            {
                search_item = "cl.Denumire";
            }
            else
            {
                search_item = "f.Nr_Contract";
            }

            cmd = new MySqlCommand("S_Facturi", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@search_item", search_item);
            cmd.Parameters.AddWithValue("@d_inceput", d_scadenta.Text);
            cmd.Parameters.AddWithValue("@d_final", d_scadenta.MaxDate.GetDateTimeFormats('u')[0]);
            cmd.Parameters.AddWithValue("@s_param",client.Text.Trim());
            DataTable table = new DataTable();
            table.Load(cmd.ExecuteReader());
            this.dataGridView1.DataSource = table;
        }

        private void client_TextChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void d_scadenta_ValueChanged(object sender, EventArgs e)
        {
            populate_grid();
        }
        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow row = dataGridView1.SelectedRows[0];
            Form f = new Edit_Factura(con, row);
            f.ShowDialog();
            client.Text = "";
            populate_grid();
        }

        private void search_SelectedIndexChanged(object sender, EventArgs e)
        {
            client.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];
            cmd = new MySqlCommand("U_F_Achitat", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", row.Cells[0].Value.ToString());

            if (row.Cells[4].Value.ToString() == "0")
            {
                cmd.Parameters.AddWithValue("@val", "1");
                row.Cells[4].Value = "1";
            }

            else
            {
                cmd.Parameters.AddWithValue("@val", "0");
                row.Cells[4].Value = "0";
            }
                
            cmd.ExecuteNonQuery();
            //populate_grid();
        }
    }
}
