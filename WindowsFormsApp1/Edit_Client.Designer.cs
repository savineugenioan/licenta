﻿namespace WindowsFormsApp1
{
    partial class Edit_Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.telefon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.IBAN = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.adresa = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.denumire = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CUI = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.client_fidel = new System.Windows.Forms.ComboBox();
            this.EditareClient = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // telefon
            // 
            this.telefon.Location = new System.Drawing.Point(125, 99);
            this.telefon.Name = "telefon";
            this.telefon.Size = new System.Drawing.Size(183, 20);
            this.telefon.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Telefon";
            // 
            // IBAN
            // 
            this.IBAN.Location = new System.Drawing.Point(125, 239);
            this.IBAN.Name = "IBAN";
            this.IBAN.Size = new System.Drawing.Size(183, 20);
            this.IBAN.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 239);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "IBAN";
            // 
            // adresa
            // 
            this.adresa.Location = new System.Drawing.Point(125, 158);
            this.adresa.Multiline = true;
            this.adresa.Name = "adresa";
            this.adresa.Size = new System.Drawing.Size(183, 68);
            this.adresa.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Adresa";
            // 
            // denumire
            // 
            this.denumire.Location = new System.Drawing.Point(125, 65);
            this.denumire.Name = "denumire";
            this.denumire.Size = new System.Drawing.Size(183, 20);
            this.denumire.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Denumire Client";
            // 
            // CUI
            // 
            this.CUI.Location = new System.Drawing.Point(125, 28);
            this.CUI.Name = "CUI";
            this.CUI.ReadOnly = true;
            this.CUI.Size = new System.Drawing.Size(183, 20);
            this.CUI.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "CUI";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Client Fidel";
            // 
            // client_fidel
            // 
            this.client_fidel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.client_fidel.FormattingEnabled = true;
            this.client_fidel.Items.AddRange(new object[] {
            "0",
            "1"});
            this.client_fidel.Location = new System.Drawing.Point(125, 131);
            this.client_fidel.Name = "client_fidel";
            this.client_fidel.Size = new System.Drawing.Size(183, 21);
            this.client_fidel.TabIndex = 26;
            // 
            // EditareClient
            // 
            this.EditareClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditareClient.Location = new System.Drawing.Point(31, 287);
            this.EditareClient.Name = "EditareClient";
            this.EditareClient.Size = new System.Drawing.Size(277, 38);
            this.EditareClient.TabIndex = 27;
            this.EditareClient.Text = "Editare Client";
            this.EditareClient.UseVisualStyleBackColor = true;
            this.EditareClient.Click += new System.EventHandler(this.EditareClient_Click);
            // 
            // Edit_Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 337);
            this.Controls.Add(this.EditareClient);
            this.Controls.Add(this.client_fidel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.telefon);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.IBAN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.adresa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.denumire);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CUI);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Edit_Client";
            this.Text = "Edit_Client";
            this.Load += new System.EventHandler(this.Edit_Client_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox telefon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox IBAN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox adresa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox denumire;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CUI;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox client_fidel;
        private System.Windows.Forms.Button EditareClient;
    }
}