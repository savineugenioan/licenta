﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class AdaugareClienti : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        public AdaugareClienti()
        {
            InitializeComponent();
        }
        public AdaugareClienti(MySqlConnection con) : this()
        {
            this.con = con;
        }

        private void AdaugareClienti_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

        }

        private void CautareClient_Click(object sender, EventArgs e)
        {
            denumire.Text = "";
            telefon.Text = "";
            adresa.Text = "";
            if(CUI.Text.Trim() == "")
            {
                MessageBox.Show("Completati CUI-ul firmei !", "CUI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!verfif_CUI(CUI.Text))
            {
                MessageBox.Show(" CUI-ul firmei este invalid!", "CUI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                /// fol api bitsoft
                string response = httpGet("http://api.bit-soft.ro/info.php?country=RO&company=" +
                  CUI.Text + "&key=4862014cf18bf9593824d368ee628ead");
                string phone = getFromTags(response, "phone");
                string name = getFromTags(response, "name");
                string country = getFromTags(response, "country");
                string city = getFromTags(response, "city");
                string address = getFromTags(response, "address");
                if (string.IsNullOrEmpty(name))
                {
                    MessageBox.Show("Firma nu a fost gasita", "Eroare Cautare", MessageBoxButtons.OK);
                    return;
                }
                denumire.Text = name;
                telefon.Text = phone;
                adresa.Text = country + ", " + city + ", " + address;
            }
            catch(Exception)
            {
                MessageBox.Show("Eroare Cautare", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public string getFromTags(string response,string tag_name)
        {
            string x;
            x = response.Substring(0, response.IndexOf("</"+tag_name+">"));
            x = x.Substring(response.IndexOf("<"+tag_name+">")+("<" + tag_name + ">").Count());
            return x;
        }

        public string httpGet(string uri)

        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())

            using (Stream stream = response.GetResponseStream())

            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }

        }

        public bool verfif_CUI(string cui)
        {
            if (!Int64.TryParse(cui,out long rez)) return false;
            if (cui.Length > 10) return false;
            long CUI = Int64.Parse(cui), v = 753217532; ;

            long c1 = CUI % 10;
            CUI /= 10;

             	// executa operatiile pe cifre
 	        long t = 0;
            while (CUI > 0){
 		    t += (CUI % 10) *(v % 10);
 		    CUI = (int)(CUI / 10);
 		    v = (int)(v / 10);
            }

            long c2 = t * 10 % 11;

            if (c2 == 10){
 		        c2 = 0;
            }
            return c1 == c2;
        }

        private void CUI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void AdaugareClient_Click(object sender, EventArgs e)
        {
            ///verificari initiale
            if (CUI.Text.Trim() == "")
            {
                MessageBox.Show("Completati CUI-ul firmei !", "CUI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (denumire.Text.Trim() == "")
            {
                MessageBox.Show("Completati Numele firmei !", "Denumire", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (adresa.Text.Trim() == "")
            {
                MessageBox.Show("Completati Adresa firmei !", "Adresa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!verfif_CUI(CUI.Text))
            {
                MessageBox.Show(" CUI-ul firmei este invalid!", "CUI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                /// Verific daca exista deja firma in baza de date

                List<string> CUIs = new List<string>();
                cmd = new MySqlCommand("Get_CUI", con);
                cmd.CommandType = CommandType.StoredProcedure;
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        CUIs.Add(rdr[0].ToString());
                    }
                }
                rdr.Close();
                if (CUIs.Contains(CUI.Text))
                {
                    MessageBox.Show(" CUI-ul firmei exista deja in baza de date!", "CUI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                /// Introducere Client. Client Fidel doarcu cont de Admin
                cmd = new MySqlCommand("Insert_Client", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CUI", CUI.Text);
                cmd.Parameters.AddWithValue("@Denumire", denumire.Text);
                cmd.Parameters.AddWithValue("@Telefon", telefon.Text);
                cmd.Parameters.AddWithValue("@Client_Fidel", "0");
                cmd.Parameters.AddWithValue("@Adresa", adresa.Text);
                cmd.Parameters.AddWithValue("@IBAN", IBAN.Text);
                if (cmd.ExecuteNonQuery() == 1)
                    MessageBox.Show("Clientul a fost introdus cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                else
                {
                    MessageBox.Show("A aparut o eroare la introducerea datelor", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine + ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
