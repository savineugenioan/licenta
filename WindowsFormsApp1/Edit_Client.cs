﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Edit_Client : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        string sql;
        string id;
        public Edit_Client()
        {
            InitializeComponent();
        }
        public Edit_Client(MySqlConnection _con, DataGridViewRow row) : this()
        {
            this.con = _con;
            id = row.Cells[0].Value.ToString();
            CUI.Text = row.Cells[1].Value.ToString();
            denumire.Text = row.Cells[2].Value.ToString();
            telefon.Text = row.Cells[3].Value.ToString();
            client_fidel.SelectedIndex = Int32.Parse(row.Cells[4].Value.ToString());
            adresa.Text = row.Cells[5].Value.ToString();
            IBAN.Text = row.Cells[6].Value.ToString();
        }


        private void Edit_Client_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
        }

        private void EditareClient_Click(object sender, EventArgs e)
        {
            ///verificari initiale
            if (CUI.Text.Trim() == "")
            {
                MessageBox.Show("Completati CUI-ul firmei !", "CUI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (denumire.Text.Trim() == "")
            {
                MessageBox.Show("Completati Numele firmei !", "Denumire", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (adresa.Text.Trim() == "")
            {
                MessageBox.Show("Completati Adresa firmei !", "Adresa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                sql = $"UPDATE CLIENTI SET CUI='{CUI.Text}',Denumire='{denumire.Text}',Telefon='{telefon.Text}'," +
                    $"Client_Fidel='{client_fidel.Text}',Adresa='{adresa.Text}',IBAN='{IBAN.Text}' WHERE Cod_Client={id};";

                cmd = new MySqlCommand("U_Clienti", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CUI", CUI.Text);
                cmd.Parameters.AddWithValue("@Denumire", denumire.Text);
                cmd.Parameters.AddWithValue("@Telefon", telefon.Text);
                cmd.Parameters.AddWithValue("@Client_Fidel", client_fidel.Text);
                cmd.Parameters.AddWithValue("@Adresa", adresa.Text);
                cmd.Parameters.AddWithValue("@IBAN", IBAN.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Clientul a fost editat cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
