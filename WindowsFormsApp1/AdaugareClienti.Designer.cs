﻿namespace WindowsFormsApp1
{
    partial class AdaugareClienti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.denumire = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.adresa = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.IBAN = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CautareClient = new System.Windows.Forms.Button();
            this.AdaugareClient = new System.Windows.Forms.Button();
            this.telefon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CUI = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "CUI";
            // 
            // denumire
            // 
            this.denumire.Location = new System.Drawing.Point(116, 87);
            this.denumire.Name = "denumire";
            this.denumire.Size = new System.Drawing.Size(183, 20);
            this.denumire.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Denumire Client";
            // 
            // adresa
            // 
            this.adresa.Location = new System.Drawing.Point(116, 161);
            this.adresa.Multiline = true;
            this.adresa.Name = "adresa";
            this.adresa.Size = new System.Drawing.Size(183, 68);
            this.adresa.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Adresa";
            // 
            // IBAN
            // 
            this.IBAN.Location = new System.Drawing.Point(116, 242);
            this.IBAN.Name = "IBAN";
            this.IBAN.Size = new System.Drawing.Size(183, 20);
            this.IBAN.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "IBAN";
            // 
            // CautareClient
            // 
            this.CautareClient.Location = new System.Drawing.Point(22, 12);
            this.CautareClient.Name = "CautareClient";
            this.CautareClient.Size = new System.Drawing.Size(277, 22);
            this.CautareClient.TabIndex = 10;
            this.CautareClient.Text = "Cautare Online ( Autocompletare )";
            this.CautareClient.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CautareClient.UseVisualStyleBackColor = true;
            this.CautareClient.Click += new System.EventHandler(this.CautareClient_Click);
            // 
            // AdaugareClient
            // 
            this.AdaugareClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdaugareClient.Location = new System.Drawing.Point(22, 286);
            this.AdaugareClient.Name = "AdaugareClient";
            this.AdaugareClient.Size = new System.Drawing.Size(277, 38);
            this.AdaugareClient.TabIndex = 11;
            this.AdaugareClient.Text = "Adaugare Client";
            this.AdaugareClient.UseVisualStyleBackColor = true;
            this.AdaugareClient.Click += new System.EventHandler(this.AdaugareClient_Click);
            // 
            // telefon
            // 
            this.telefon.Location = new System.Drawing.Point(116, 121);
            this.telefon.Name = "telefon";
            this.telefon.Size = new System.Drawing.Size(183, 20);
            this.telefon.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Telefon";
            // 
            // CUI
            // 
            this.CUI.Location = new System.Drawing.Point(116, 50);
            this.CUI.Name = "CUI";
            this.CUI.Size = new System.Drawing.Size(183, 20);
            this.CUI.TabIndex = 1;
            this.CUI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CUI_KeyPress);
            // 
            // AdaugareClienti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(368, 359);
            this.Controls.Add(this.telefon);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AdaugareClient);
            this.Controls.Add(this.CautareClient);
            this.Controls.Add(this.IBAN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.adresa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.denumire);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CUI);
            this.Controls.Add(this.label1);
            this.Name = "AdaugareClienti";
            this.Text = "Adaugare Clienti";
            this.Load += new System.EventHandler(this.AdaugareClienti_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox denumire;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox adresa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox IBAN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button CautareClient;
        private System.Windows.Forms.Button AdaugareClient;
        private System.Windows.Forms.TextBox telefon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CUI;
    }
}