﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Office.Interop.Excel;
using MySql.Data.MySqlClient;
using DataTable = System.Data.DataTable;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp1
{
    public partial class R_Testare : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        DataGridViewRow selectedRow;
        DataTable table;
        public R_Testare()
        {
            InitializeComponent();
        }
        public R_Testare(MySqlConnection con) :this()
        {
            this.con = con;
        }

        private void R_Testare_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            search.SelectedItem = "DENUMIRE CLIENT";
            d_scadenta.Format = DateTimePickerFormat.Custom;
            d_scadenta.CustomFormat = "yyyy-MM-dd";
            d_scadenta.Value = DateTime.Today.AddMonths(-3);

            try
            {
                populate_grid();
                dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void search_SelectedIndexChanged(object sender, EventArgs e)
        {
            client.Text = "";
        }

        private void client_TextChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void d_scadenta_ValueChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            string path;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }

            try
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                selectedRow = dataGridView1.Rows[selectedrowindex];
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


                xlWorkSheet.get_Range("A1", "A1").Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWorkSheet.get_Range("A1", "A1").Style.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlWorkSheet.get_Range("a1", "e2").Merge(false);
                xlWorkSheet.Cells[1, 1] = "Raport de Testare";
                xlWorkSheet.get_Range("a3", "e3").Merge(false);
                xlWorkSheet.Cells[3, 1] = $"Clientul : {dataGridView1.Rows[selectedrowindex].Cells[3].Value.ToString()}";
                xlWorkSheet.Cells[1, 1].Font.Size = 20;
                xlWorkSheet.Cells[1, 1].Font.Bold = true;
                xlWorkSheet.Cells[5, 1] = "Nr.Crt.";
                xlWorkSheet.Cells[5, 2] = "Procedura";
                xlWorkSheet.Cells[5, 3] = "R.Asteptat";
                xlWorkSheet.Cells[5, 4] = "Pass";
                xlWorkSheet.Cells[5, 5] = "Codul Testarii";
                var chartRange = xlWorkSheet.get_Range("a4", "e4");
                chartRange.Columns.AutoFit();
                xlWorkSheet.Columns[2].ColumnWidth = 45;//procedura

                //get Data
                cmd = new MySqlCommand("S_Testari_Detalii", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", selectedRow.Cells[0].Value.ToString());
                rdr = cmd.ExecuteReader();
                int i = 6;
                int str = i;
                while (rdr.Read())
                {
                    //populare tabel
                    xlWorkSheet.Cells[i, 1] = rdr[0].ToString();
                    xlWorkSheet.Cells[i, 2] = rdr[1].ToString();
                    xlWorkSheet.Cells[i, 3] = rdr[2].ToString();
                    xlWorkSheet.Cells[i, 4] = rdr[3].ToString();
                    xlWorkSheet.Cells[i, 5] = rdr[4].ToString();
                    i++;
                }
                rdr.Close();

                //formatari tabel
                Range rng = xlWorkSheet.get_Range("A5", $"E{i - 1}");
                rng.WrapText = true;
                rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                //pt print repeat
                xlWorkSheet.PageSetup.PrintTitleRows = "$1:$4";

                //salvare fisier
                xlWorkBook.SaveAs($"{path}\\Raport_de_Testare.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                MessageBox.Show($"Fisier XL creat : {path}\\Raport_de_Testare.xls");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Cannot access"))
                {
                    MessageBox.Show($"Fisierul XL nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                        $"Raport_de_Testare.xls si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void populate_grid()
        {
            string search_item;
            if (search.SelectedItem.ToString() == "DENUMIRE CLIENT")
            {
                search_item = "cl.Denumire";
            }
            else
            {
                search_item = "t.Nr_Contract";
            }

            table = new DataTable();
            cmd = new MySqlCommand("S_Testari", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@search_item", search_item);
            cmd.Parameters.AddWithValue("@d_inceput", d_scadenta.Text);
            cmd.Parameters.AddWithValue("@d_final", d_scadenta.MaxDate.GetDateTimeFormats('u')[0]);
            cmd.Parameters.AddWithValue("@s_param", client.Text.Trim());
            table.Load(cmd.ExecuteReader());
            this.dataGridView1.DataSource = table;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string path;
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    path = fbd.SelectedPath;
                }
                else return;
            }
            try
            {
                if (File.Exists($"{ path}\\Raport_de_Testare.pdf"))
                {
                    DialogResult result = MessageBox.Show("Fisierul deja exista. Doriti sa il rescrieti?", "Confirmare", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (result == DialogResult.No)
                    {
                        return;
                    }
                }
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                selectedRow = dataGridView1.Rows[selectedrowindex];
                FileStream fs = new System.IO.FileStream($"{path}\\Raport_de_Testare.pdf", System.IO.FileMode.Create);

                Document document = new Document(PageSize.A4, 25, 25, 30, 30);


                PdfWriter writer = PdfWriter.GetInstance(document, fs);

                // Open the document to enable you to write to the document  
                document.Open();
                // Add a simple and wellknown phrase to the document in a flow layout manner  
                Paragraph x = new Paragraph("Raport de Testare", boldFont);
                x.Alignment = Element.ALIGN_CENTER;
                document.Add(x);
                x = new Paragraph($"Clientul : {dataGridView1.Rows[selectedrowindex].Cells[3].Value.ToString()}");
                x.Alignment = Element.ALIGN_CENTER;
                document.Add(x);
                document.Add(new Paragraph(" "));
                PdfPTable table1 = new PdfPTable(5);
                table1.HeaderRows = 1;

                table1.AddCell("Nr.Crt.");
                table1.AddCell("Procedura");
                table1.AddCell("R.Asteptat");
                table1.AddCell("Pass");
                table1.AddCell("Codul Testarii");
                cmd = new MySqlCommand("S_Testari_Detalii", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", selectedRow.Cells[0].Value.ToString());
                rdr = cmd.ExecuteReader();
                int i = 6;
                int str = i;
                while (rdr.Read())
                {
                    //populare tabel
                    table1.AddCell(rdr[0].ToString());
                    table1.AddCell(rdr[1].ToString());
                    table1.AddCell(rdr[2].ToString());
                    table1.AddCell(rdr[3].ToString());
                    table1.AddCell(rdr[4].ToString());
                }
                rdr.Close();

                document.Add(table1);
                document.Close();
                writer.Close();
                fs.Close();
                MessageBox.Show($"Fisier PDF creat : {path}\\Raport_de_Testare.pdf");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("cannot access"))
                {
                    MessageBox.Show($"Fisierul PDF nu poate fi accesat. Va rugam sa verificati daca este deja deschis un fisier cu numele " +
                        $"Raport_de_Testare.pdf si sa il inchideti", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (ex.Message.Contains("Exception from HRESULT: 0x800A03EC"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
