﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class AdaugareTestat : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;

        public AdaugareTestat()
        {
            InitializeComponent();
        }
        public AdaugareTestat(MySqlConnection _con) : this()
        {
            this.con = _con;
        }

        private void AdaugareTestat_Load(object sender, EventArgs e)
        {
            data.Format = DateTimePickerFormat.Custom;
            data.CustomFormat = "yyyy-MM-dd";

            DateTime dateTime = DateTime.UtcNow.Date;

            dateTime = data.Value;
            try
            {


                if (con.State == ConnectionState.Open)
                {

                    // CALCULEZ NUMARUL
                    cmd = new MySqlCommand("Max_Index_Testari", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    int i = (int)cmd.ExecuteScalar() + 1;
                    numar.Text = i.ToString();

                }
                else
                {
                    MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        // EVENT verific daca e numar
        private void Not_Number_Press(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }



        private void nr_contract_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //verificari de inceput
            if (Int32.TryParse(nr_contract.Text, out int res) == false)
            {
                MessageBox.Show("Introduceti Numarul Contractului  ( Corect! )", "Nr Contract", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (testari.Rows.Count == 1 && Globalz.EmptyCellsNoAdd(testari, 2))
            {
                MessageBox.Show("Toate celulele trebuie sa contina date !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Globalz.EmptyCells(testari,2))
            {
                MessageBox.Show("Toate celulele trebuie sa contina date !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var rows = testari.Rows;

            if (con.State == ConnectionState.Open)
            {
                try
                {
                    //Insert in TESTARE
                    cmd = new MySqlCommand("I_Testari", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Cod_Testare", numar.Text);
                    cmd.Parameters.AddWithValue("@Data_Test", data.Text);
                    cmd.Parameters.AddWithValue("@Nr_Contract", nr_contract.Text);
                    int x = cmd.ExecuteNonQuery();

                    for (int j = 0; j < rows.Count - 1; j++)
                    {
                        string[] cells = new string[3];
                        for (int i = 0; i < 3; i++)
                        {
                            cells[i] = rows[j].Cells[i].EditedFormattedValue.ToString();
                            if (i == 2)
                            {
                                if (cells[i] == "True")
                                    cells[i] = "1";
                                else
                                    cells[i] = "0";
                            }

                        }

                        //Insert in Testare_Detalii
                        cmd = new MySqlCommand("I_Testari_Detalii", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Procedura", cells[0]);
                        cmd.Parameters.AddWithValue("@Rezultatul_Asteptat", cells[1]);
                        cmd.Parameters.AddWithValue("@Pass", cells[2]);
                        cmd.Parameters.AddWithValue("@Cod_Testare", numar.Text);
                        x = cmd.ExecuteNonQuery();

                    }
                    MessageBox.Show("Datele au fost introduse cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                catch (Exception err)
                {
                    if (err.Message.Contains("Cannot add or update a child row: a foreign key constraint fails"))
                    {
                        MessageBox.Show($"Contractul nu exista in baza de date ! ", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        MessageBox.Show(err.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nr_contract_TextChanged(object sender, EventArgs e)
        {
            testari.Visible = false;
            button1.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cmd = new MySqlCommand("Verif_Contract", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nr_Contr", nr_contract.Text);
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                testari.Visible = true;
                button1.Visible = true;
            }
            else
            {
                testari.Visible = false;
                button1.Visible = false;
                MessageBox.Show("Contractul nu exista in baza de date", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            rdr.Close();
        }
    }
  
}
