﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class Globalz
    {
        public static bool EmptyCells(DataGridView dataGrid, params int[] excp)
        {
            for (int i = 0; i < dataGrid.Rows.Count - 1; i++)
            {
                foreach (DataGridViewCell cell in dataGrid.Rows[i].Cells)
                    if(!excp.Contains(cell.ColumnIndex))
                {
                    if (string.IsNullOrEmpty(cell.EditedFormattedValue as string))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool EmptyCellsNoAdd(DataGridView dataGrid, params int[] excp)
        {
            for (int i = 0; i < dataGrid.Rows.Count; i++)
            {
                foreach (DataGridViewCell cell in dataGrid.Rows[i].Cells)
                    if (!excp.Contains(cell.ColumnIndex))
                    {
                        if (string.IsNullOrEmpty(cell.EditedFormattedValue as string))
                        {
                            return true;
                        }
                    }
            }
            return false;
        }
        public static double getEuro()
        {
            var webClient = new WebClient();
            string curs = webClient.DownloadString("https://www.cursbnr.ro");
            curs = curs.Substring(curs.IndexOf("1 EURO = "));
            curs = curs.Substring(curs.IndexOf('=') + 2, 5);
            return double.Parse(curs);
        }
        public static void LoadingGif(object y)
        {
            try
            {
                Control x = (Control)y;
                Form f = x.FindForm();
                PictureBox gif = new PictureBox();
                gif.Image = Properties.Resources.loader_gif_transparent_background_1;
                gif.Location = new Point(x.Location.X + x.Width + 10, x.Location.Y);
                gif.SizeMode = PictureBoxSizeMode.StretchImage;
                gif.Size = new Size(x.Height - 5, x.Height - 5);
                f.Controls.Add(gif);
            }
            catch { };
        }

        public static void del_gif(Form f)
        {
            foreach (PictureBox item in f.Controls.OfType<PictureBox>())
                if (item.Name == "")
                {
                    f.Controls.Remove(item);
                }
        }
    }

}
