﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AdaugareContracte : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        public AdaugareContracte()
        {
            InitializeComponent();
        }

        public AdaugareContracte(MySqlConnection con) : this()
        {
            this.con = con;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void adaugare_contract_Click(object sender, EventArgs e)
        {
            //verificari initiale
            if (lista_clienti.SelectedIndex < 0)
            {
                MessageBox.Show("Selectati un Client din Lista", "Client", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (avans.Text.Trim() == "" || Int32.Parse(avans.Text) <=0)
            {
                MessageBox.Show("Introduceti o suma pentru avans ( >0 ) !", "Avans", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (total.Text.Trim() == "" || Int32.Parse(total.Text) <= 0)
            {
                MessageBox.Show("Introduceti o suma pentru Restul de Plata ( >0 ) !", "Rest de Plata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int avans_platit = 0, contract_platit = 0, suma_avans,suma_total;
            string d_incheiere, d_avans, d_lichidare;
            string client;


            if(avans_ck.CheckState == CheckState.Checked)
            {
                avans_platit = 1;
            }
            if (contract_ck.CheckState == CheckState.Checked)
            {
                contract_platit = 1;
            }
            d_incheiere = data_incheiere.Text;
            d_avans = data_avans.Text;
            d_lichidare = data_lichidare.Text;
            suma_avans = int.Parse(avans.Text);
            suma_total = int.Parse(total.Text);
            client = lista_clienti.SelectedItem.ToString();

            cmd = new MySqlCommand("Insert_Contract", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Data_Incheierii", d_incheiere);
            cmd.Parameters.AddWithValue("@Avans", suma_avans);
            cmd.Parameters.AddWithValue("@Avans_Platit", avans_platit);
            cmd.Parameters.AddWithValue("@Rest_Plata", suma_total);
            cmd.Parameters.AddWithValue("@Contract_Platit", contract_platit);
            cmd.Parameters.AddWithValue("@Data_Plata_Avans", d_avans);
            cmd.Parameters.AddWithValue("@Data_Plata_Rest", d_lichidare);
            cmd.Parameters.AddWithValue("@Cod_Cl", client);
            if (con.State == ConnectionState.Open)
            {
                try
                {
                    //Insert inContracte
                    if (cmd.ExecuteNonQuery() == 1)
                        MessageBox.Show("Datele au fost introduse cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                    else
                    {
                        MessageBox.Show("A aparut o eroare la introducerea datelor", "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    this.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
            else
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //
        }

        private void AdaugareContracte_Load(object sender, EventArgs e)
        {
            data_incheiere.Format = DateTimePickerFormat.Custom;
            data_incheiere.CustomFormat = "yyyy-MM-dd";
            data_avans.Format = DateTimePickerFormat.Custom;
            data_avans.CustomFormat = "yyyy-MM-dd";
            data_lichidare.Format = DateTimePickerFormat.Custom;
            data_lichidare.CustomFormat = "yyyy-MM-dd";

            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

        }

        private void avans_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void total_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareClienti(con);
            f.ShowDialog();
        }

        private void text_clienti_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //luam clientii din baza de date
                Dictionary<int, string> clienti = new Dictionary<int, string>();
                lista_clienti.Items.Clear();
                cmd = new MySqlCommand("S_Client_Like", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@den", text_clienti.Text);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        string x = rdr[1].ToString();
                        clienti.Add(Int32.Parse(rdr[0].ToString()), rdr[1].ToString());
                        lista_clienti.Items.Add(rdr[1].ToString());
                    }
                }
                rdr.Close();

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
