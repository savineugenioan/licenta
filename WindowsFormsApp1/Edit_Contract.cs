﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace WindowsFormsApp1
{
    public partial class Edit_Contract : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        string id;
        public Edit_Contract()
        {
            InitializeComponent();
        }
        public Edit_Contract(MySqlConnection _con, DataGridViewRow row) : this()
        {
            this.con = _con;
            id = row.Cells[0].Value.ToString();
            nr_contract.Text = row.Cells[0].Value.ToString();
            data_incheiere.Text = row.Cells[1].Value.ToString();
            avans.Text = row.Cells[2].Value.ToString();
            total.Text = row.Cells[4].Value.ToString();
            avans_ck.Checked = getCheck(row.Cells[3].Value.ToString());
            contract_ck.Checked = getCheck(row.Cells[5].Value.ToString());
        }

        private void Edit_Contract_Load(object sender, EventArgs e)
        {
            data_incheiere.Format = DateTimePickerFormat.Custom;
            data_incheiere.CustomFormat = "yyyy-MM-dd";

            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
        }
        private bool getCheck(string x)
        {
            if (x == "0")
                return false;
            return true;
        }
        private int getCheckInt(bool x)
        {
            if (x == false)
                return 0;
            return 1;
        }

        private void adaugare_contract_Click(object sender, EventArgs e)
        {
            try
            {
                cmd = new MySqlCommand("U_Contracte", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Avans_Platit", getCheckInt(avans_ck.Checked));
                cmd.Parameters.AddWithValue("@Contract_Platit", getCheckInt(contract_ck.Checked));
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contractul a fost editat cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
