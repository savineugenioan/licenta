﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace WindowsFormsApp1
{
    public partial class Edit_Achizitie : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        string id;
        public Edit_Achizitie()
        {
            InitializeComponent();
        }
        public Edit_Achizitie(MySqlConnection _con, DataGridViewRow row) : this()
        {
            this.con = _con;
            id = row.Cells[0].Value.ToString();
            serie_numar.Text = id;
        }

        private void Edit_Achizitie_Load(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            try
            {
                DataTable table = new DataTable();
                cmd = new MySqlCommand("S_Achizitie", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                table.Load(cmd.ExecuteReader());
                this.dataGridView1.DataSource = table;
                dataGridView1.Columns[0].ReadOnly = true;
                dataGridView1.Columns[5].ReadOnly = true;
                dataGridView1.Columns[6].ReadOnly = true;
                dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Globalz.EmptyCellsNoAdd(dataGridView1))
            {
                MessageBox.Show("Toate celulele trebuie sa contina date !", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                double suma = 0;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    double pret = double.Parse(row.Cells[3].Value.ToString()) * double.Parse(row.Cells[4].Value.ToString());
                    suma += pret;

                    cmd = new MySqlCommand("U_Achizitii_Detalii", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Produsul", row.Cells[1].Value.ToString());
                    cmd.Parameters.AddWithValue("@U_M", row.Cells[2].Value.ToString());
                    cmd.Parameters.AddWithValue("@Cantitatea", row.Cells[3].Value.ToString());
                    cmd.Parameters.AddWithValue("@Pretul_Unitar", row.Cells[4].Value.ToString());
                    cmd.Parameters.AddWithValue("@Pret", pret);
                    cmd.Parameters.AddWithValue("@Cod_Det", row.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                }

                cmd = new MySqlCommand("U_Achizitii", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@suma", suma);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Achizitia a fost editata cu succes !", "SUCCES", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Not_Number_Press);
            if (dataGridView1.CurrentCell.ColumnIndex == 3 || dataGridView1.CurrentCell.ColumnIndex == 4)
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Not_Number_Press);
                }
            }
        }

        private void Not_Number_Press(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentCell.ColumnIndex == 3 || dataGridView1.CurrentCell.ColumnIndex == 4)
                {
                    if (dataGridView1.CurrentRow.Cells[3].EditedFormattedValue.ToString() != "" && dataGridView1.CurrentRow.Cells[4].EditedFormattedValue.ToString() != "")
                    {
                        dataGridView1.CurrentRow.Cells[5].Value = double.Parse(dataGridView1.CurrentRow.Cells[3].EditedFormattedValue.ToString()) * double.Parse(dataGridView1.CurrentRow.Cells[4].EditedFormattedValue.ToString());

                    }
                    else
                    {
                        dataGridView1.CurrentRow.Cells[5].Value = 0;
                    }
                }
            }
            catch { }
        }
    }
}
