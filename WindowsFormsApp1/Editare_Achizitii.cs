﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Editare_Achizitii : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        public Editare_Achizitii()
        {
            InitializeComponent();
        }
        public Editare_Achizitii(MySqlConnection _con) :this()
        {
            this.con = _con;
        }

        private void Editare_Achizitii_Load(object sender, EventArgs e)
        {            
            if (con.State != ConnectionState.Open)
            {
                MessageBox.Show("CONEXIUNE ESUATA LA BAZA DE DATE" + Environment.NewLine, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            search.SelectedItem = "DENUMIRE CLIENT";
            d_inceput.Format = DateTimePickerFormat.Custom;
            d_inceput.CustomFormat = "yyyy-MM-dd";
            d_inceput.Value = DateTime.Today.AddMonths(-4);
            d_final.Format = DateTimePickerFormat.Custom;
            d_final.CustomFormat = "yyyy-MM-dd";
            d_final.Value = DateTime.Today;
            

            try
            {
                populate_grid();
                dataGridView1.Columns[2].Width = 200;
                dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void populate_grid()
        {
            string search_item;
            if (search.SelectedItem.ToString() == "DENUMIRE CLIENT")
            {
                search_item = "cl.Denumire";
            }
            else
            {
                search_item = "c.Nr_Contract";
            }
            cmd = new MySqlCommand("S_Achizitii", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@search_item", search_item);
            cmd.Parameters.AddWithValue("@d_inceput", d_inceput.Text);
            cmd.Parameters.AddWithValue("@d_final", d_final.Text);
            cmd.Parameters.AddWithValue("@s_param", client.Text.Trim());

            DataTable table = new DataTable();
            table.Load(cmd.ExecuteReader());
            this.dataGridView1.DataSource = table;
        }

        private void d_inceput_ValueChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void d_final_ValueChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void client_TextChanged(object sender, EventArgs e)
        {
            populate_grid();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow row = dataGridView1.SelectedRows[0];
            Form f = new Edit_Achizitie(con, row);
            f.ShowDialog();
            client.Text = "";
            populate_grid();
        }

        private void search_SelectedIndexChanged(object sender, EventArgs e)
        {
            client.Text = "";
        }
    }
}
