﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Get_Id_Client : Form
    {
        public string ReturnValue1 { get; set; }
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataReader rdr;
        public Get_Id_Client()
        {
            InitializeComponent();
        }
        public Get_Id_Client(MySqlConnection con) :this()
        {
            this.con = con;
        }

        private void Get_Id_Client_Load(object sender, EventArgs e)
        {
            populadeList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new AdaugareClienti(con);
            f.ShowDialog();
            populadeList();
        }
        private void populadeList()
        {
            listBox1.Items.Clear();
            ReturnValue1 = "";
            cmd = new MySqlCommand("S_Client_Like", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@den", "");
            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                listBox1.Items.Add(rdr[0].ToString() + " " + rdr[1].ToString());
            }
            rdr.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Selectati un Client din Lista !!!");
                return;
            }
            var str = listBox1.SelectedItem.ToString();
            ReturnValue1 = str.Substring(0, str.IndexOf(' '));
            this.Close();
                
        }
    }
}
